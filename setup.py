import sys
import os
import glob
import shutil

path_exec=os.path.dirname(  __file__ )
if not path_exec :
    path_exec='.'
sys.path.append(path_exec + os.sep + 'pysiril' + os.sep + 'lib')
import changelog

# ------------------------------------------------------------------------------
# clean build directories and files
def clean_build(dirlist=None,filelist=None):
    if dirlist is not None :
        for  dirglb in dirlist :
             for  dir in glob.glob( dirglb ):
                print("* clean:",dir )
                shutil.rmtree(dir,ignore_errors=True)
    if filelist is not None :
        for  fileglb in filelist :
            for  fic in glob.glob( fileglb ) :
                print("* clean:",fic )
                try:
                    os.unlink(fic)
                except:
                    pass
# ------------------------------------------------------------------------------      
def package_setuptools():
    from setuptools import setup, find_packages
    with open("README.md", "r") as fh:
        long_description = fh.read()
        
    list_install_requires = [ 'pypiwin32 ; platform_system=="Windows"' ]

    setup(
        name = "pysiril",
        version = changelog.NO_VERSION,
        author="M27trognondepomme",
        author_email="M27trognondepomme@wordpress.com",
        license='LGPL-v3',
        description='pySiriL is a python library for SiriL scripting.',
        long_description=long_description,
        long_description_content_type="text/markdown",
        url="https://gitlab.com/free-astro/pysiril.git",
        packages=find_packages(),
        python_requires='>=3.6',
        classifiers=[
            "Programming Language :: Python :: 3",
            "Development Status :: 5 - Production/Stable",
            "Programming Language :: Python :: 3.6",
            "Topic :: Scientific/Engineering :: Astronomy",
            "License :: OSI Approved :: GNU General Public License v3 (GPLv3)"
        ],
        install_requires=list_install_requires,
        #entry_points={
        #    'console_scripts': [ 'pysiril=pysiril.siril', ],
        #  },
        include_package_data=True
    )

# ------------------------------------------------------------------------------
if len(sys.argv ) == 1 :
    print( "Build Package:" )
    print( "    o python source package :" )
    print( "        > python setup.py sdist" )
    print( "")
    print( "    o python (*.whl) package :" )
    print( "        > python setup.py bdist_wheel " )
    print( "")
    print( "    o debian (*.deb) package :" )
    print( "        > python setup.py bdist_deb" )
    print( "")
    exit( 0 )
    
clean_build( [ 'build','dist','deb_dist', '*egg-info' ], ['*tar.gz','pysiril*.spec' ] )
package_setuptools()
clean_build( [ 'build', '*egg-info' ], ['*tar.gz' ] )
    
