import sys
import os


try:
    from pysiril.siril   import *
    from pysiril.wrapper import *
    from pysiril.addons  import *
except:
    print(" ***  Mode Developement - la lib pysiril n'est pas installe *** ")
    path_exec=os.path.dirname(  __file__ )
    if not path_exec :
        path_exec='.'
    sys.path.append(path_exec + os.sep + '..')
    from siril   import *
    from wrapper import *
    from addons  import *

app=Siril()
cmd=Wrapper(app)

help( Siril )               # pour la doc des fonctions de base de la librairie
help( Wrapper )             # pour l'aide des fonctions wrapper *** pas fini ***
help( Addons )              # pour l'aide des fonctions annexes

cmd.help( 'convertraw')     # pour l'aide d'une fonction Siril
cmd.help()                  # pour l'aide de toutes les fonctions SiriL

app.Close()
del app
