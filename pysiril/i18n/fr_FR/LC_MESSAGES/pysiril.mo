��    �      �  �   �	        |        �     �  	   �  �   �  B   �  A   �  ~     ?  �  f   �  �   >  \    O   l  0   �  �   �  /   �     �    �  ?   �  �   $  �   �  �   �  /  {  >  �     �  J  �    :     O   �  ]   �   �!  �  �"  T   w'  U   �'  �   "(  U   �(    )  ;   %*  "   a*     �*     �*  n   �*  Y   ++  +   �+  _   �+  h   ,     z,  ^  �,     �-     �-  �   .  �  �.  �  �1  H   3  =   _3     �3  1   �3  "  �3      5  �  !9  B   =     D=     S=  �   k=  O   ,>  (   |>    �>  {   �?  g   4@  �   �@  <    A  B   ]A  >   �A  *   �A  -   
B  f   8B  {   �B     C     )C     7C  �   EC  �   �C  �   �D  �   'E  �   �E  Z   {F  �   �F  �   ~G  �   kH  �   I  ?   �I  �   �I  O   tJ  �   �J  �   sK  :   L    WL  #   \M     �M  �   �N    cO  i   vP  X   �P     9Q  S  OQ  k   �R  �   S  B   	T  .   LT      {T  $   �T     �T  ^   �T  �   %U     �U  G   �U      �U  )   V     EV  3   ZV  �   �V     PW  @  XW  �  �_     �a     �a     �a     �a     �a  
   �a  
   �a  
   b     b     b      b     #b     =b     Wb     qb     �b  +   �b     �b     �b     �b     c     
c     %c     Dc     ac     {c     �c     �c  #   �c  	   �c     �c    �c     e     �e     �e     �e    �e  O   �f  V   �f  �   Pg  _  �g  o   Pi    �i  �  �j  U   �m  1   �m  �   n  3   �n     o  <  o  D   Vp  �   �p  �   �q  &  (r  �  Os  �  �u     ux  e  {x  J  �y     ,{  �  :{    }  �  ~  j   ��  e   �  �   r�  t   �  d  ��  D   �  %   7�  $   ]�     ��  �   ��  b   '�  .   ��  p   ��  r   *�     ��  �  ��     U�     d�    i�    k�  �  ��  F   E�  ?   ��     ̑  2   ڑ  O  �    ]�  �  t�  N   �     R�     a�  �   x�  `   X�  7   ��  '  �  �   �  v   ��  �   (�  ;   ��  F   ��  =   B�  /   ��  1   ��  s   �  }   V�     ԣ     �     �  �   ��  �   ֤  �   ��  �   Y�  �   	�  j   ��  �   (�    �  �   ��  �   ��  I   6�  �   ��  \   .�  �   ��  �   f�  ?   '�  *  g�  7   ��  i  ʯ  �   4�  0  �  q   H�  X   ��     �  �  .�  y   ��  /  w�  T   ��  .   ��  "   +�  )   N�     x�  l   ��  �   ��     ��  G   ��  (   ٹ  -   �     0�  K   N�  �   ��     |�  
  ��  �  ��  
   6�     A�     ]�     u�     ��     ��     ��     ��     ��     ��     ��     ��     ��     
�     $�  '   >�  1   f�     ��     ��     ��     ��     ��     ��  %   �      <�     ]�     z�     ��  %   ��  
   ��     ��     �      3   @   M       a      }      �   /      G       �       :   V                   "   8       T      $      L   +   �       z           �              s   A               #       ?      �       �       �      c           {               .                  0   Y       �   -   O       F      K      W   H   _   g             �   n       B      �   Z   �                              �   
   f   �   i   w   b           N       ^              �       k          2   l   h   E       S   o   I       d   �       q   X      �   �   9      '   |         &   (   ,   �   �   P      	   x   >      !   u   7   �       5       �   m       Q   U   [   �   %   t   p       `       ]   D          �       =   �   R   ~           4   �   1   e      y   \   �   C   6      )   ;      <   *   r   v       j      J   �     
Stretches the image to show faint objects, while simultaneously, 
preserves the structure of bright objects of the field.
 ... aborted Aborted Aborted : Adds the constant value (specified in ADU) to the current image. This constant 
can take a negative value.
In 16-bit mode, values of pixels that fall outside of [0, 65535] are clipped. 
In 32-bit mode, no clipping occurs.
 Adds the image in memory to the image filename given in argument.
 Allow images to be saved with 32 bits per channel on processing.
 Applies a Fast Fourier Transform to the image loaded in memory. Modulus and phase 
given in argument are saved in FITS files.
 Applies a chromatic noise reduction filter. It removes green noise in the current 
image. This filter is based on PixInsight's SCNR Average Neutral algorithm and 
it is the same filter used by HLVG plugin in Photoshop.

Type=1 stands for Average Neutral Protection, while type=2 stands for Maximum 
Neutral Protection.
 Applies an automatic detection of cold and hot pixels following the thresholds 
written in arguments.
 Applies midtones transfer function to the current loaded image.
Three parameters are needed, low, midtones and high where midtones balance 
parameter defines a nonlinear histogram stretch in the [0,1] range.
 Applies the local mean to a set of pixels on the in-memory image (cosmetic correction). 
The coordinates of these pixels are in an ASCII file [.lst file]. COSME is adapted 
to correct residual hot and cold pixels after preprocessing")

The line P x y type will fix the pixel at coordinates (x, y) type is an optional 
character (C or H) specifying to Siril if the current pixel is cold or hot. This 
line is created by the command find_hot but you also can add some lines manually.
The line C x 0 type will fix the bad column at coordinates x.
The line L y 0 type will fix the bad line at coordinates y.
 Applies to the working image an unsharp mask with sigma and coefficient multi.
 Are you sure you want to overwrite image files?  Calculates the histogram of the image channel in memory and produces file 
histo_[channel name].dat in the working directory".

layer = 0, 1 or 2 with 0=red, 1=green and 2=blue.
 Check if the "Developer Mode" option is enabled Cleaning :  Computes a linear function between a reference image and the image in memory.
The function is then applied to the current image to match it to the reference one. 
The algorithm will ignore all reference pixels whose values are outside of the 
[low, high] range".
 Computes and applies a logarithmic scale to the current image.
 Computes the entropy of the opened image on the displayed layer, 
only in the selected area if one has been selected or in the whole image. 
The entropy is one way of measuring the noise or the details in an image.
 Computes the level of the local sky background thanks to a polynomial function 
of an order degree and subtracts it from the image.
 Computes the wavelet transform on "nbr_plan" plans using linear (type=1) or 
bspline (type=2) version of the 'a trous' algorithm. The result is stored in a 
file as a structure containing the planes, ready for weighted reconstruction 
with WRECONS.
 Converts DSLR RAW files into Siril's FITS images.
    o The argument basename is the basename of the new sequence. 
      For FITS images, Siril will try to make a symbolic link. 
      If not possible, files will be copied.
    o The flags -fitseq and -ser can be used to specify an alternative output 
      format, other than the default FITS.
    o The option -debayer applies demosaicing to images. 
      In this case no symbolic link are done.
    o -start=index sets the starting index number 
    o -out= option converts files into the directory out
 Converts all images in a known format into Siril's FITS images.
    o The argument basename is the basename of the new sequence. 
      For FITS images, Siril will try to make a symbolic link. 
      If not possible, files will be copied.
    o The flags -fitseq and -ser can be used to specify an alternative output 
      format, other than the default FITS.
    o The option -debayer applies demosaicing to images. 
      In this case no symbolic link are done.
    o -start=index sets the starting index number 
    o -out= option converts files into the directory out.
 Copy Creates two images, with a radial shift (dR in pixels) and a rotational shift 
(dalpha in degrees) with respect to the point (xc, yc).

Between these two images, the shifts have the same amplitude, but an opposite sign. 
The two images are then added to create the final image. This process is also 
called Larson Sekanina filter
 Crops a selection of the loaded image.
In the GUI, if a selection is active, no further arguments are required. 
Otherwise, or in scripts, arguments have to be given, with x and y being 
the coordinates of the top left corner, and width and height the size of 
the selection.
 DESCRIPTION:  Defines if images are compressed or not. 
0 means no compression while 1 enables compression.
If compression is enabled, the type must be explicitly written in the option 
-type= ("rice", "gzip1", "gzip2").
Associated to the compression, the quantization value must be within [0, 256] 
range. For example, "setcompress 1 -type=rice 16" sets the rice compression with 
a quantization of 16
 Defines the number of processing threads used for calculation.

Can be as high as the number of virtual threads existing on the system, which is 
the number of CPU cores or twice this number if hyperthreading (Intel HT) is available.
 Defines thresholds above the noise and star roundness for stars detection with 
FINDSTAR and REGISTER commands.

Sigma must be greater or equal to 0.05 and roundness between 0 and 0.9

The threshold for star detection is computed as the median of the image (which 
represents in general the background level) plus Ksigma times sigma, sigma being 
the standard deviation of the image (which is a good indication of the noise
 amplitude). If you have many stars in your images and a good signal/noise ratio, 
 it may be a good idea to increase this value to speed-up the detection and false 
 positives.

The roundness argument is the minimal ratio of the short axis on the long axis 
of the star Gaussian fit (see PSF). A value of 1 would keep only perfectly round 
stars, a value of 0.5, the default, means that stars detected twice as big on an 
axis as on the other will still be used for registration.

It is recommended to test the values used for a sequence with Siril's GUI, 
available in the dynamic PSF toolbox from the analysis menu. It may improve 
registration quality to increase the parameters, but it is also important to be 
able to detect several tens of stars in each image.
 Disallow images to be saved with 32 bits per channel on processing, use 16 instead.
 Divides the image in memory by the image filename given in argument. 
See also FDIV.
 Divides the image in memory by the image given in argument. The resulting image 
is multiplied by the value of the scalar argument. See also idiv.
 Enhances the global saturation of the image. Try iteratively to obtain best results.
 Equalizes the histogram of an image using Contrast Limited Adaptive Histogram Equalization.
   o cliplimit sets the threshold for contrast limiting.
   o tilesize sets the size of grid for histogram equalization. 
Input image will be divided into equally sized rectangular tiles.
 Equalizes the mean intensity of RGB layers in a CFA image.
 Error import win32pipe & win32file Error, python version should be Error: Siril don't work Extracts Ha and OIII signals from a CFA image. The output file name start with 
the prefix "Ha_" and "OIII_".
 Extracts Ha signal from a CFA image. The output file name starts with the 
prefix "Ha_".
 Extracts NbPlans Planes of Wavelet domain.
 Extracts green signal from a CFA image. The output file name starts with the 
prefix "Green_".
 Fills the whole current image (or selection) with pixels having the value 
intensity expressed in ADU..
 First step: Starting Fixes the Fujifilm X-Trans Auto Focus pixels.
Indeed, because of the phase detection auto focus system, the photosites used 
for auto focus get a little less light than the surrounding photosites. The 
camera compensates for this and increases the values from these specific 
photosites giving a visible square in the middle of the dark/bias frames.
 Initialisation Link Links all FITS images in the working directory with the basename given in argument.
If no symbolic links could be created, files are copied. It is possible to convert 
files in another directory with the -out= option.
 Loads the image filename
It first attempts to load filename, then filename.fit, finally filename.fits 
and finally all supported formats, aborting if none of these are found.
This scheme is applicable to every Siril command that involves reading files.
Fits headers MIPS-HI and MIPS-LO are read and their values given to the current 
viewing levels.
Writing a known extension .ext at the end of filename will load specifically the 
image filename.ext: this is used when numerous files have the same name but not 
the same extension.

Extensions supported are :
    *.fit, *.fits, *.fts
    *.bmp / *.jpg, *.jpeg / *.png / *.tif, *.tiff
    *.ppm, *.pgm
    *.pic (IRIS file)
 Merges several sequences of the same type (FITS images, FITS sequence or SER) 
and same image properties into a new sequence with base name newseq created in 
the current working directory, with the same type. 
The input sequences can be in different directories, can specified either 
in absolute or relative path, with the exact .seq name or with only 
the base name with or without the trailing '_'. 
 Multiplies the image in memory by the image filename given in argument.
 Multiplies the loaded image by the scalar given in argument.
 NAME       :  Performs a Gaussian filter with the given sigma.
 Performs a median filter of size ksize x ksize (ksize MUST be odd) to the  
original image with a modulation parameter modulation.
The output pixel is computed as : out=mod x m+(1−mod) x in, where m is the 
median-filtered pixel value. A modulation's value of 1 will apply no modulation.
 Performs geometric transforms on images of the sequence given in argument so that 
they may be superimposed on the reference image. Using stars for registration, 
this algorithm only works with deepsky images.

o The output sequence name starts  with the prefix "r_" unless otherwise specified 
with -prefix= option.

o The option -drizzle activates the sub-pixel stacking, either by up-scaling by 2 
the images created in the rotated sequence or by setting a flag that will proceed 
to the up-scaling during stacking if -norot is passed.

o The option -transf= specifies the use of either "shift", "similarity", "affine" 
  or "homography" transformations respectively, homography being the default 
  unless -norot is passed, which uses shift as default.
  
o The option -minpairs= will specify the minimum number of star pairs a frame must 
  have with the reference frame, otherwise the frame will be dropped.

o The registration is done on the green layer for RGB images unless specified by 
  -layer= option (0, 1 or 2 for R, G and B respectively).

 Preprocesses the sequence sequencename using bias, dark and flat given in argument

o For bias, a uniform level can be specified instead of an image, by entering a quoted 
expression starting with an = sign, such as -bias="=256" or  -bias="=64*$OFFSET".

o It is possible to specify if images are CFA for cosmetic correction purposes 
with the option -cfa and also to demosaic images at the end of the process with 
-debayer.

o The -fix_xtrans option is dedicated to X-Trans files by applying a correction 
on darks and biases to remove an ugly square pattern.

o The -equalize_cfa option equalizes the mean intensity of RGB layers of the CFA 
flat master.

o It is also possible to optimize the dark subtraction with -opt.

o The output sequence name starts with the prefix "pp_" unless otherwise specified
with option -prefix=.

o If -fitseq is provided, the output sequence will be a FITS sequence (single file).

Note that only hot pixels are corrected in cosmetic correction process.

 Properly closes the opened image and the opened sequence, if any.
 Python version Quits the application.
 Reconstructs to current image from the planes previously computed with wavelets 
and weighted with coefficients c1, c2, ..., cn according to the number of planes 
used for wavelet transform. 
 Replaces null values by level values. Useful before an idiv or fdiv operation.
 Resamples image with a factor "factor".
 Restores an image using the Richardson-Lucy method.

o Sigma is the size of the kernel to be applied, while corner_radius_boost is a 
 value which is added to Gaussian sigma for the tiles in the corners of an image.
o Iterations is the number of iterations to be performed

 Retrieves corrected image applying an inverse transformation. 
The modulus and phase used are the files given in argument.
 Returns an error if the version of Siril is older than the one passed in argument.
ie: requires 0.99.6
 Returns global statistic of the current image. If a selection is made, the command 
returns global statistic within the selection.
 Returns the background level of the image loaded in memory.
 Returns the background noise level of the image loaded in memory.
 Returns the coordinates of the center of gravity of the image
 Rotates the image around a vertical axis.
 Rotates the image around an horizontal axis.
 Rotates the image by an angle of degree value. 
The option -nocrop can be added to avoid the cropping
 Rotates the image of an angle of 180° around its center. This is equivalent to 
the command "ROTATE 180" or "ROTATE -180"
 STATUS     :  SYNTAXE1   :  SYNTAXE2   :  Same command as COSME but for the the sequence sequencename.

The output sequence name starts with the prefix "cosme_" unless otherwise  
specified with option "-prefix="
 Same command as COSME but for the the sequence sequencename.
The output sequence name starts with the prefix "cosme_" unless otherwise 
specified with option "-prefix="
 Same command as FILL but this is a symmetric fill of a region defined by the mouse. 
Used to process an image in the Fourier (FFT) domain.
 Same command as FIND_COSME but for the sequence sequencename.
The output sequence name starts with the prefix "cc_" unless otherwise specified 
with -prefix= option .
 Same command as FIND_COSME_CFA but for the sequence sequencename.
The output sequence name starts with the prefix "cc_" unless otherwise specified 
with -prefix= option .
 Same command as SAVETIF but the output file is saved in 32-bit per channel: filename.tif.
 Same command as SPLIT_CFA but for the sequence sequencename. The output sequences 
names start with the prefix "CFA_" unless otherwise specified with -prefix= option.
 Same command as STAT for sequence sequencename.
The output is saved as a csv file given in second argument.
The optional parameter defines the number of statistical values computed: basic 
or main (more detailed but longer to compute).
 Same command as SUBSKY but for the sequence sequencename. The output sequence 
name starts with the prefix "bkg_" unless otherwise specified with -prefix= option.
 Same command than EXTRACT_HAOIII but for the sequence "seqname". The output 
sequence name start with the prefix "Ha_" and "OIII_".
 Same command than FIND_COSME but for monochromatic CFA images.
 Same command than MTF but for the sequence seqname. The output sequence name 
starts with the prefix "mtf_" unless otherwise specified with "-prefix=" option.
 Same command than SAVE_TIF but the output file is saved in 32bits per channel.
 Same command than extract_Green but for the sequence "seqname". 
The output sequence name starts with the prefix "Green_" unless otherwise 
specified with option "-prefix=".
 Same command than extract_Ha but for the sequence "seqname". 
The output sequence name starts with the prefix "Ha_" unless otherwise 
specified with option "-prefix=".
 Same function that COSME but applying to RAW CFA images. 
 Saves a list file filename (text format) in the working directory which contains 
the coordinates of the pixels which have an intensity hot_sigma times higher and 
cold_sigma lower than standard deviation. We generally use this command on a 
master-dark file.
 Saves current image as a PNG file.
 Saves current image into a JPG file: filename.jpg.

You have the possibility to adjust the quality of the compression. A value 100 
for quality parameter offers best fidelity while a low value increases the 
compression ratio. If no value is specified, a default value of 100 is applied.
 Saves current image to filename.fit (or .fits, depending on your preferences, 
see SETEXT). Fits headers MIPS-HI and MIPS-LO are added with values corresponding 
to the current viewing levels.
 Saves current image under the form of a Netpbm file format with 16-bit per channel.

The extension of the output will be filename.ppm for RGB image and filename.pgm 
for gray-level image

More details about the Netpbm format at : http://en.wikipedia.org/wiki/Netpbm_format.
 Saves current image under the form of a bitmap file with 8bits per channel: 
filename.bmp (BMP 24 bits).
 Saves current image under the form of a uncompressed TIFF file with 16bits per channel.
 Second step: Starting Sets a new ratio of free memory on memory used for stacking.
Ratio value should be between 0.05 and 2, depending on other activities of the 
machine. A higher ratio should allow siril to stack faster, but setting the ratio 
of memory used for stacking above 1 will require the use of on-disk memory, which 
is very slow and unrecommended.
 Sets the extension used and recognized by sequences.
The argument extension can be "fit", "fts" or "fits".
 Sets the new current working directory.
The argument directory can contain the ~ token, expanded as the home directory, 
directories with spaces in the name can be protected using single or double quotes
Examples:
    cd ~/M42
    cd '../OIII 2x2/'
 Sets the reference image of the sequence given in first argument.
 Shows the negative view of the current image.
 Siril is compatible with pysiril Siril is not compatible with pySiril Skip Splits the CFA image into four distinct files (one for each channel) and 
save them in files.
 Splits the color image into three distinct files (one for each color) and save 
them in fileR.fit, fileG.fit and fileB.fit files")
 Stopping Subtracts the image in memory by the image filename given in argument.
 Symbolic Link Error on Windows:  The module pywin32 should be downloaded : Third step: Starting Timeout expired : Problem to run 'Siril --version'  Tries to remove the canon banding.
    o Argument amount define the amount of correction. 
    o Sigma defines a protection level of the algorithm, higher sigma gives 
      higher protection.
 VERSION With [filtering] being some of these in no particular order or number:
     [-filter-fwhm=value[%]] [-filter-wfwhm=value[%]] [-filter-round=value[%]]
     [-filter-quality=value[%]] [-filter-included]    

Stacks the seqfilename sequence, using options.
    o Rejection type:
        The allowed types are: "sum", "max", "min", "med" (or "median") 
        and "rej" (or "mean"). If no argument other than the sequence name is 
        provided, sum stacking is assumed.
    o Stacking with rejection:
        Types rej or mean require the use of additional arguments for rejection 
        type and values. The rejection type is one of {p[ercentile] | s[igma]
         | m[edian] | w[insorized] | l[inear] | g[eneralized] | [m]a[d]} for 
        Percentile, Sigma, Median, Winsorized, Linear-Fit, Generalized Extreme 
        Studentized Deviate Test or k-MAD clipping. If omitted, the default 
        (Winsorized) is used. The sigma low and sigma high parameters of rejection 
        are mandatory.
    o Normalization of input images:
        For med|median and rej|mean stacking types, different types of 
        normalization are allowed: -norm=add for additive, -norm=mul for 
        multiplicative. Options -norm=addscale and -norm=mulscale apply same 
        normalization but with scale operations. 
        "-nonorm" is the option to disable normalization. Otherwise addtive with 
        scale method is applied by default.

    o Other options for rejection stacking:
        "-weighted" is an option to add larger weights to frames with lower 
        background noise.
        "-output_norm" applies a normalization at the end of the stacking to 
        rescale result in the [0, 1] range.
        
    o Outputs:
        Result image name can be set with the -out= option. Otherwise, it will 
        be named as sequencename_stacked.fit

    Filtering out images:
        Images to be stacked can be selected based on some filters, like manual 
        selection or best FWHM, with some of the -filter-* options.
    See the command reference for the complete documentation on this command
 With filtering being some of these in no particular order or number:
     [-filter-fwhm=value[%]] [-filter-wfwhm=value[%]] [-filter-round=value[%]]
     [-filter-quality=value[%]] [-filter-incl[uded]]

Opens all sequences in the current directory and stacks them with the optionally 
specified stacking type and filtering or with sum stacking. See STACK command for 
options description.

Stacked images for each sequence are created with the suffix "_stacked" and the 
configured FITS file extension.
 aborted commands of pysiril/siril :
 directory doesn't exist don't exist incoherent extension is aborted is started is stopped no not processed or pySiril requires : Siril  pysiril is already closed pysiril is already opened pysiril is not ready pysiril is not ready or closed pysiril needs the path  of executable siril pysiril uses by default : run siril don't exist skipping status[0] = False: Aborted status[0] = True: execution ok status[1] = background level status[1] = entropy value status[1] = statistics status[1] = x  status[2] = y  unknown commands of pysiril/siril 
 waiting:  yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-11-23 17:49+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
Plural-Forms: nplurals=2; plural=(n > 1);
  
Étire l’image pour montrer des objets faibles, tout en même temps, 
préserve la structure des objets lumineux du champ.
 … Annuler Echec Echec: Ajoute la valeur constante (spécifiée dans ADU) à l’image actuelle. Cette constante 
peut prendre une valeur négative.
En mode 16 bits, les valeurs des pixels qui se situent en dehors de [0, 65535] sont coupées. 
En mode 32 bits, aucun écrêtage ne se produit.
 Ajoute l’image en mémoire au nom de fichier image donné dans l’argument.
 Permettre aux images d'être enregistrées avec 32 bits par canal lors du traitement.
 Applique une transformation de Fourier rapide à l'image chargée en mémoire. Module et phase
données en argument sont enregistrées dans des fichiers FITS.
 Applique un filtre chromatique de réduction du bruit. Il supprime le bruit vert dans l'image 
courante. Ce filtre est basé sur l’algorithme SCNR Average Neutral de PixInsight et 
c’est le même filtre utilisé par le plugin HLVG dans Photoshop.

Type=1 signifie Average Neutral Protection, tandis que type=2 signifie Maximum 
Protection neutre.
 Applique une détection automatique des pixels froids et chauds suivant les seuils 
écrit dans les arguments.
 Applique la fonction de transfert des tons moyens à l’image courante chargée .
Trois paramètres sont nécessaires, les tons bas, moyens et élevés là où les tons moyens s’équilibrent 
définit un étirement d’histogramme non linéaire dans la plage [0,1].
 Applique la moyenne locale à un ensemble de pixels sur l’image en mémoire (correction cosmétique). 
Les coordonnées de ces pixels se trouvent dans un fichier ASCII [fichier .lst]. COSME est adapté 
pour corriger les pixels chauds et froids résiduels après prétraitement »)

Le type de ligne P x y fixera le pixel aux coordonnées (x, y) est facultatif 
caractère (C ou H) spécifiant à Siril si le pixel actuel est froid ou chaud. Ceci 
est créée par la commande find_hot mais vous pouvez également ajouter quelques lignes manuellement.
Le type de ligne C x 0 corrigera la colonne incorrecte aux coordonnées x.
Le type de ligne L y 0 corrigera la mauvaise ligne aux coordonnées y.
 Applique à l'image de travail un masque flou avec sigma sigma et coefficient multi.
 Etes-vous sûr de vouloir d'écraser les images?  Calcule l’histogramme du canal d’image en mémoire et produit le fichier 
histo_[nom du canal].dat dans le répertoire de travail ».

calque = 0, 1 ou 2 avec 0 = rouge, 1 = vert et 2 = bleu.
 Vérifiez si l'option "Developer Mode" est activée Nettoyage:  Calcule une fonction linéaire entre une image de référence et l’image en mémoire.
La fonction est ensuite appliquée à l’image actuelle pour la faire correspondre à celle de référence. 
L’algorithme ignorera tous les pixels de référence dont les valeurs sont en dehors de la
plage [basse, haute] ».
 Calcule et applique une échelle logarithmique à l'image actuelle.
 Calcule l’entropie de l’image ouverte sur le calque affiché, 
uniquement dans la zone sélectionnée si elle a été sélectionnée ou dans l’ensemble de l’image. 
L’entropie est une façon de mesurer le bruit ou les détails d’une image.
 Calcule le niveau de l’arrière-plan du ciel local grâce à une fonction polynomiale 
d’un degré d’ordre et le soustrait de l’image.
 Calcule la transformée en ondelettes sur les plans "nbr_plan" en utilisant linéaire (type = 1) ou
version bspline (type = 2) de l'algorithme 'a trous'. Le résultat est stocké dans un
fichier sous forme de structure contenant les plans, prêt pour la reconstruction pondérée
avec WRECONS.
 Convertit les fichiers RAW (APN) en images FITS de Siril.
    o L’argument basename est le nom de base de la nouvelle séquence. 
      Pour les images FITS, Siril va essayer de faire un lien symbolique. 
      Si ce n’est pas possible, les fichiers seront copiés.
    o Les indicateurs '-fitseq' et '-ser' peuvent être utilisés pour spécifier une sortie alternative 
      autre que le FITS par défaut.
    o L’option '-debayer' démosaïque les images. 
      Dans ce cas, aucun lien symbolique n’est fait.
    o -start=index définit le numéro d’index de départ 
    o -out= option convertit les fichiers dans le répertoire out
 Convertit toutes les images dans un format connu en images FITS de Siril.
    o L’argument basename est le nom de base de la nouvelle séquence. 
      Pour les images FITS, Siril va essayer de faire un lien symbolique. 
      Si ce n’est pas possible, les fichiers seront copiés.
    o Les indicateurs '-fitseq' et '-ser' peuvent être utilisés pour spécifier une sortie alternative 
       autre que le FITS par défaut.
    o L’option '-debayer' démosaïque les images. 
      Dans ce cas, aucun lien symbolique n’est fait.
    o -start=index définit le numéro d’index de départ 
    o -out= option convertit les fichiers dans le répertoire out.
 Copie Crée deux images, avec un décalage radial ("dR" en pixels) et un décalage de rotation
("dalpha" en degrés) par rapport au point ("xc", "yc"). Entre ces deux images,
les décalages ont la même amplitude, mais un signe opposé. Les deux images
sont ensuite ajoutés pour créer l'image finale. Ce processus est également appelé Larson
Filtre Sekanina.
 Recadre selon la sélection l’image chargée.
Dans l’interface graphique, si une sélection est active, aucun autre argument n’est requis. 
Sinon, ou dans les scripts, les arguments doivent être donnés, x et y étant 
les coordonnées du coin supérieur gauche, et la largeur et la hauteur de la taille de 
la sélection.
 DESCRIPTION:  Définit si les images sont compressées ou non. 
0 signifie pas de compression tandis que 1 active la compression.
Si la compression est activée, le type doit être explicitement écrit dans l’option 
-type= (« rice», « gzip1 », « gzip2 »).
Associée à la compression, la valeur de quantification doit se situer dans [0, 256] 
gamme. Par exemple, « setcompress 1 -type=rice 16 » définit la compression rice avec 
une quantification de 16
 Définit le nombre de threads de traitement utilisés pour le calcul.

Peut être aussi élevé que le nombre de threads virtuels existants sur le système, qui est 
le nombre de cœurs de processeur ou deux fois ce nombre si l’hyperthreading (Intel HT) est disponible.
 Définit des seuils au-dessus du bruit et de la rondeur des étoiles pour la détection des étoiles avec 
Commandes FINDSTAR et REGISTER.

Sigma doit être supérieur ou égal à 0,05 et arrondi entre 0 et 0,9

Le seuil de détection d’étoiles est calculé comme la médiane de l’image (qui 
représente en général le niveau de fond) plus K.sigma fois sigma, sigma étant 
l’écart-type de l’image (qui est une bonne indication du bruit
 amplitude). Si vous avez beaucoup d’étoiles dans vos images et un bon rapport signal/bruit, 
 il peut être judicieux d’augmenter cette valeur pour accélérer la détection et les faux 
 Positifs.

L’argument de rondeur est le rapport minimal de l’axe court sur l’axe long 
de l’étoile gaussienne (voir PSF). Une valeur de 1 ne garderait que parfaitement ronde 
étoiles, une valeur de 0,5, la valeur par défaut, signifie que les étoiles détectées deux fois plus grandes sur un 
l’axe comme sur l’autre sera toujours utilisé pour l’enregistrement.

Il est recommandé de tester les valeurs utilisées pour une séquence avec l’interface graphique de Siril, 
disponible dans la boîte à outils PSF dynamique à partir du menu d’analyse. Il peut s’améliorer 
qualité d’enregistrement pour augmenter les paramètres, mais il est également important d’être 
capable de détecter plusieurs dizaines d’étoiles dans chaque image.
 Interdire l'enregistrement des images avec 32 bits par canal lors du traitement, utilisez 16 à la place.
 Divise l’image en mémoire par le nom de fichier image donné dans l’argument. 
Voir aussi FDIV.
 Divise l’image en mémoire par l’image donnée dans l’argument. L’image résultante 
est multiplié par la valeur de l’argument scalaire. Voir aussi idiv.
 Améliore la saturation globale de l’image. Essayez de manière itérative pour obtenir les meilleurs résultats.
 Égalise l’histogramme d’une image à l’aide de l’égalisation adaptative de l’histogramme à contraste limité.
   o 'cliplimit' définit le seuil de limitation du contraste.
   o 'tilesize' définit la taille de la grille pour l’égalisation de l’histogramme. 
L’image d’entrée sera divisée en tuiles rectangulaires de taille égale.
 Égalise l’intensité moyenne des calques RVB dans une image CFA.
 Erreur "import win32pipe & win32file" Erreur, la version python doit être Erreur: SIRIL ne fonctionne pas Extrait les signaux Ha et OIII d’une image CFA. Le nom du fichier de sortie commence par 
les préfixes « Ha_ » et « OIII_ ».
 Extrait le signal Ha d'une image CFA. Le nom du fichier de sortie commence par le préfixe "Ha_".
 Extrait les NbPlan plans du filtre Ondelette.
 Extrait le signal vert d’une image CFA. Le nom du fichier de sortie commence par le 
préfixe « Green_ ».
 Remplit toute l'image (ou la sélection) actuelle avec des pixels ayant la valeur 
d'intensité exprimé en  ADU.
 Première étape: Démarrage Corrige les pixels fujifilm X-Trans Auto Focus.
En effet, en raison la détection de phase du système de mise au point automatique, les photosites utilisés 
pour la mise au point automatique ont un peu moins de lumière que les sites photo environnants. Le 
la caméra compense cela et augmente les valeurs de ces photosites spécifiques 
donnant ainsi un carré visible au milieu des images de Dark/Offset.
 Initialisation Lien Lie toutes les images FITS du répertoire de travail avec le nom de base indiqué dans l’argument.
Si aucun lien symbolique n’a pu être créé, les fichiers sont copiés. Il est possible de convertir 
dans un autre répertoire avec l’option '-out='.
 Charge l'image donné en argument
Il tente d’abord de charger filename, puis filename.fit, enfin filename.fits 
et enfin tous les formats pris en charge, abandonnant si aucun d’entre eux n’est trouvé.
Ce schéma est applicable à toutes les commandes Siril qui impliquent la lecture de fichiers.
S’adapte aux en-têtes MIPS-HI et MIPS-LO sont lus et leurs valeurs données au courant 
niveaux d’affichage.
L’écriture d’une extension connue .ext à la fin du nom de fichier chargera spécifiquement le 
image filename.ext : il est utilisé lorsque de nombreux fichiers ont le même nom mais pas 
la même extension.

Les extensions prises en charge sont :
    *.fit, *.fits, *.fts
    *.bmp / *.jpg, *.jpeg / *.png / *.tif, *.tiff
    *.ppm, *.pgm
    *.pic (fichier IRIS)
 Fusionne plusieurs séquences du même type (images FITS, séquence FITS ou SER)
et les mêmes propriétés d'image dans une nouvelle séquence avec le nom de base newseq créé dans
le répertoire de travail actuel, avec le même type.
Les séquences d'entrée peuvent être dans différents répertoires, peuvent être spécifiées soit
en chemin absolu ou relatif, avec le nom exact .seq ou avec seulement
le nom de base avec ou sans le '_' de fin.
 Multiplie l’image en mémoire par l'image donné dans l’argument.
 Multiplie l'image chargée par le scalaire donné en argument.
 NAME       :  Effectue un filtre gaussien avec le sigma donné.
 Effectue un filtre médian de taille ksize x ksize (ksize DOIT être impair) à la  
image originale avec une modulation de paramètre de modulation.
Le pixel de sortie est calculé comme : out=mod x m+(1−mod) x in, où m est le 
valeur de pixel filtrée médiane. La valeur de 1 d’une modulation n’appliquera aucune modulation.
 Effectue des transformations géométriques sur des images de la séquence donnée dans l’argument de sorte que 
ils peuvent être superposés à l’image de référence. Utilisation d’étoiles pour l’enregistrement, 
cet algorithme ne fonctionne qu’avec des images de ciel profond.

o Le nom de la séquence de sortie commence par le préfixe « r_ », sauf indication contraire. 
avec -prefix= option.

o L’option '-drizzle' active l’empilement des sous-pixels, soit en augmentant la mise à l’échelle de 2 
les images créées dans la séquence pivotée ou en définissant un indicateur qui se poursuivra 
à la mise à l’échelle pendant l’empilement si -norot est passé.

o L’option -transf= spécifie l’utilisation de « shift », « similarity », « affine » 
  ou transformations « homographiques » respectivement, l’homographie étant la valeur par défaut 
  sauf si '-norot' est passé, ce qui utilise shift par défaut.
  
o L’option '-minpairs'= spécifiera le nombre minimum de paires d’étoiles qu’une image doit 
  avoir avec l'image de référence, sinon le l'image sera rejeté.

o L’alignement se fait sur la couche verte pour les images RVB, sauf indication contraire de 
  '-layer= option' (0, 1 ou 2 pour R, G et B respectivement).

 Prétraite le nom de séquence de séquence à l’aide d’un offset, dark et flat donné dans l’argument

o Pour le offset, un niveau uniforme peut être spécifié à la place d’une image,  en écrivant  un signe '='
   puis une expression entre  guillemet, tel que -bias="=256 » ou -bias="=64*$OFFSET ».

o Il est possible de préciser si les images sont CFA à des fins de correction cosmétique 
avec l’option '-cfa' et aussi aux images débayrisées à la fin du processus avec 
'-debayer'.

o L’option -fix_xtrans est dédiée aux fichiers X-Trans en appliquant une correction 
sur les darks et les offsets pour supprimer le motif carré de l'image.

o L’option '-equalize_cfa' égalise l’intensité moyenne des couches RVB du Master Flat CFA 

o Il est également possible d’optimiser la soustraction du dark avec '-opt'.

o Le nom de la séquence de sortie commence par le préfixe « pp_ », sauf indication contraire
avec l’option '-prefix='.

o Si '-fitseq' est fourni, la séquence de sortie sera une séquence FITS (fichier unique).

Notez que seuls les pixels chauds sont corrigés dans le processus de correction cosmétique.

 Ferme correctement l'image ouverte et la séquence ouverte, le cas échéant.
 Version Python Quitte l'application.
 Reconstruit l'image actuelle à partir des plans précédemment calculés avec des ondelettes
et pondéré avec les coefficients c1, c2, ..., cn en fonction du nombre de plans
utilisé pour la transformation en ondelettes.
 Remplace les valeurs nulles par des valeurs de niveau. Utile avant une opération idiv ou fdiv.
 Rééchantillonne l'image avec un facteur «facteur».
 Restaure une image à l’aide de la méthode Richardson-Lucy.

o Sigma est la taille du noyau à appliquer, tandis que corner_radius_boost est un 
 valeur qui est ajoutée au sigma gaussien pour les tuiles dans les coins d’une image.
o Itérations est le nombre d’itérations à effectuer

 Récupère l’image corrigée en appliquant une transformation inverse. 
Le module et la phase utilisés sont les fichiers donnés dans l’argument.
 Renvoie une erreur si la version de Siril est antérieure à celle transmise dans l’argument.
ie: nécessite 0.99.6
 Renvoie la statistique globale de l'image actuelle. Si une sélection est effectuée, la commande
renvoie une statistique globale dans la sélection.
 Renvoie le niveau du fond de l'image chargée en mémoire.
 Renvoie le niveau de bruit de fond de l’image chargée en mémoire.
 Renvoie les coordonnées du centre de gravité de l’image.
 Fait pivoter l'image autour d'un axe vertical.
 Fait pivoter l'image autour d'un axe horizontal.
 Fait pivoter l’image d’un angle en degré. 
L’option '-nocrop' peut être ajoutée pour éviter le recadrage
 Fait pivoter l'image d'un angle de 180 ° autour de son centre. Cela équivaut à
la commande "ROTATE 180" ou "ROTATE -180".
 STATUS     :  SYNTAXE1   :  SYNTAXE2   :  Même commande que COSME mais pour le nom de séquence de séquence.

Le nom de la séquence de sortie commence par le préfixe « cosme_ », sauf indication contraire.  
spécifié avec l’option « -prefix= »
 Même commande que COSME mais pour le nom de séquence de séquence.
Le nom de la séquence de sortie commence par le préfixe « cosme_ », sauf indication contraire. 
spécifié avec l’option « -prefix= »
 Même commande que FILL mais il s'agit d'un remplissage symétrique d'une région définie par le
Souris. Utilisé pour traiter une image dans le domaine de Fourier (FFT).
 Même commande que FIND_COSME mais pour une sequence .
Le nom de la séquence de sortie commence par le préfixe « cc_ », sauf indication contraire. 
avec -prefix= option .
 Même commande que FIND_COSME_CFA mais pour une séquence.
Le nom de la séquence de sortie commence par le préfixe « cc_ », sauf indication contraire. 
avec -prefix= option .
 Même commande que SAVETIF mais le fichier de sortie est enregistré en 32 bits par canal : filename.tif.
 Même commande que SPLIT_CFA mais pour une  séquence. Les séquences de sortie 
les noms commencent par le préfixe « CFA_ », sauf indication contraire avec l’option -prefix=.
 Même commande que STAT mais pour une sequence.
La sortie est enregistrée sous la forme d’un fichier csv donné dans le deuxième argument.
Le paramètre facultatif définit le nombre de valeurs statistiques calculées : basic 
ou main (plus détaillé mais plus long à calculer).
 Même commande que SUBSKY mais pour une séquence. La séquence de sortie
nom commence par le préfixe "bkg_" sauf indication contraire avec l'option "-prefix =".
 Même commande que EXTRACT_HAOIII mais pour la séquence "seqname". Le résultat
le nom de la séquence commence par le préfixe "Ha_" et "OIII_".
 Même commande que FIND_COSME mais pour les images CFA monochromatiques.
 Même commande que MTF mais pour la séquence seqname. Le nom de la séquence de sortie
commence par le préfixe "mtf_" sauf indication contraire avec l'option "-prefix =".
 Même commande que SAVE_TIF mais le fichier de sortie est enregistré en 32 bits par canal.
 Même commande que extract_Green sauf pour la séquence « seqname ». 
Le nom de la séquence de sortie commence par le préfixe « Green_ », sauf indication contraire 
spécifié avec l’option « -prefix= ».
 Même commande que extract_Ha mais pour la séquence "seqname".
Le nom de la séquence de sortie commence par le préfixe "Ha_" sauf indication contraire
spécifié avec l'option "-prefix =".
 Même fonction que COSME mais s'appliquant aux images RAW CFA.
 Enregistre une liste de fichier ( format texte) dans le répertoire de travail qui contient 
les coordonnées des pixels qui ont une intensité 'hot_sigma' fois plus élevée et 
'cold_sigma'  inférieur à l’écart-type. Nous utilisons généralement cette commande sur un 
fichier Dark-master.
 Enregistre l'image actuelle sous forme de fichier PNG.
 Enregistre l’image actuelle dans un fichier JPG : filename.jpg.

Vous avez la possibilité d’ajuster la qualité de la compression. Une valeur 100 
pour le paramètre de qualité offre la meilleure fidélité tandis qu’une valeur faible augmente le 
taux de compression. Si aucune valeur n’est spécifiée, une valeur par défaut de 100 est appliquée.
 Enregistre l’image actuelle dans filename.fit (ou .fits, selon vos préférences, 
voir SETEXT). S’adapte aux en-têtes MIPS-HI et MIPS-LO sont ajoutés avec des valeurs correspondantes 
aux niveaux d’affichage actuels.
 Enregistre l’image actuelle sous la forme d’un format de fichier Netpbm avec 16 bits par canal.

L’extension de la sortie sera filename.ppm pour l’image RVB et filename.pgm 
pour l’image de niveau de gris

Plus de détails sur le format Netpbm sur : http://en.wikipedia.org/wiki/Netpbm_format.
 Enregistre l'image courante sous la forme d'un fichier bitmap avec 8 bits par canal:
filename.bmp (BMP 24 bits).
 Saves current image under the form of a uncompressed TIFF file with 16bits per channel.
 Seconde étape: Démarrage Définit un nouveau ratio de mémoire libre sur la mémoire utilisée pour l’empilement.
La valeur du ratio devrait être comprise entre 0,05 et 2, selon les autres activités de la
machine. Un rapport plus élevé devrait permettre au siril de s’empiler plus rapidement, mais en définissant le rapport 
de la mémoire utilisée pour l’empilement au-dessus de 1 nécessitera l’utilisation de mémoire sur disque, qui 
est très lent et non-recommandé.
 Définit l'extension utilisée et reconnue par les séquences. L'argument "extension" peut
être "fit", "fts" ou "fits".
 Définissez le nouveau répertoire de travail actuel. 
Le répertoire peut contenir le tilde ~, il sera développé comme répertoire de base.
Les répertoires avec des espaces dans le nom peuvent être protégé par des guillemets 
simples ou doubles. Exemples:
     cd ~ / M42
     cd '../OIII 2x2 /'
 Définit l’image de référence de la séquence donnée dans le premier argument.
 Affiche la vue négative de l'image actuelle.
 Siril est  compatible avec pySiril Siril n’est pas compatible avec pySiril Ignorer Divise l'image CFA en quatre fichiers distincts (un pour chaque canal) et les enregistre dans des fichiers.
 Divise l'image couleur en trois fichiers distincts (un pour chaque canal) et 
les enregistre dans les fichiers :  fileR.fit, fileG.fit and fileB.fit files.
 Arrêt Soustrait l’image en mémoire avec l'image donné dans l’argument.
 Erreur de lien symbolique sur Windows :  Le module "pywin32" doit être téléchargé: Troisième étape: Démarrage Délai d’attente expiré : Problème d’exécution de 'Siril --version'  Tentes de supprimer les bandes de APN de type CANON
    o l'argument 'amount' définit la quantité de correction. 
    o Sigma définit le niveau de protectionde  l'algorithme, 
       un sigma élevé augment la protection
 VERSION Avec [filtering] étant l'une d'entre elles, sans ordre ni nombre particulier :
     [-filter-fwhm=value[%]] [-filter-wfwhm=value[%]] [-filter-round=value[%]]
     [-filter-quality=value[%]] [-filter-included]    

Empile la séquence seqfilename, en utilisant les options.
    o Type de rejet :
        Les types autorisés sont : "sum", "max", "min", "med" (ou "median"). 
        et "rej" (ou "mean"). Si aucun argument autre que le nom de la séquence n'est 
        est fourni, l'empilement de la somme est supposé.
    o Empilage avec rejet :
        Les types rej ou mean nécessitent l'utilisation d'arguments supplémentaires pour le rejet 
        type et valeurs. Le type de rejet est l'un des suivants : {p[ercentile] | s[igma] | m[edian] | m[edian] | m[edian].
         | m[edian] | w[insorized] | l[inear] | g[eneralized] | [m]a[d]} pour les types suivants 
        Percentile, sigma, médiane, winsorisé, ajustement linéaire, extrême généralisé. 
        Test d'écart studentisé ou écrêtage k-MAD. Si elle est omise, la valeur par défaut 
        (Winsorized) est utilisé. Les paramètres sigma low et sigma high de rejet 
        sont obligatoires.
    o Normalisation des images d'entrée :
        Pour les types d'empilage med|médian et rej|moyen, différents types de 
        types de normalisation sont autorisés : -norm=add pour additif, -norm=mul pour multiplicatif. 
        multiplicatif. Les options -norm=addscale et -norm=mulscale appliquent les mêmes 
        normalisation mais avec des opérations d'échelle. 
        "-nonorm" est l'option pour désactiver la normalisation. Sinon, la méthode addtive avec 
        scale est appliquée par défaut.

    o Autres options pour l'empilement des rejets :
        "-weighted" est une option qui permet d'ajouter des poids plus importants aux images avec un faible 
        bruit de fond.
        "-output_norm" applique une normalisation à la fin de l'empilement pour 
        pour redimensionner le résultat dans la plage [0, 1].
        
    o Sorties :
        Le nom de l'image du résultat peut être défini avec l'option -out=. Sinon, il sera 
        Sinon, elle sera nommée sequencename_stacked.fit.

    Filtrage des images :
        Les images à empiler peuvent être sélectionnées en fonction de certains filtres, comme la sélection manuelle ou le meilleur FWHM, avec certaines options. 
        manuelle ou le meilleur FWHM, avec certaines des options -filter-*.
    Voir la référence de la commande pour la documentation complète sur cette commande.
 Avec [filtering] étant l'une d'entre elles, sans ordre ni nombre particulier :
     [-filter-fwhm=value[%]] [-filter-wfwhm=value[%]] [-filter-round=value[%]]
     [-filter-quality=value[%]] [-filter-included]    

Ouvre toutes les séquences dans le répertoire courant et les empile avec le type d'empilement et le filtrage spécifiés en option ou avec l'empilement de la somme. 
type d'empilement et le filtrage spécifiés en option ou avec l'empilement par somme. Voir la commande STACK pour 
description des options.

Des images empilées pour chaque séquence sont créées avec le suffixe "_stacked" et l'extension de fichier 
l'extension de fichier FITS configurée.
 abandonné commandes de pysiril/siril
 le dossier n'existe pas n’existent pas extension incohérente est abandonné est démarré est arrêté non pas traité ou pySiril exige: Siril  pysiril est déjà fermé pysiril est déjà ouvert pysiril n’est pas prêt pysiril n’est pas prêt ou est fermé pysiril a besoin du chemin de l'exécutable SiriL pysiril utilise par défaut: run siril n'existe pas passe au suivant status[0] = False: Echec status[0] = True: exécution ok status[1] = niveau du fond de l'image status[1] = valeur de l'entropie status[1] = les statistiques status[1] = x  status[2] = y  commandes inconnus de  pysiril/siril
 attendre:  oui 