# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: pysiril ( Python SiriL )
#
# This is a library to interface python to Siril.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
from __future__ import absolute_import
__all__=["Wrapper"]
import sys
import os
import re
import time

path_exec=os.path.dirname(  __file__ )

if not path_exec :
    path_exec='.'
sys.path.append(path_exec + os.sep + 'lib')
import help

# ------------------------------------------------------------------------------
class Wrapper:
    def __init__(self, sirilpipe ):
        
        self.siril       = sirilpipe
        self.tr          = self.siril.tr
    
    # --------------------------------------------------------------------------
    def Execute(self, commandes, bEndTest = True):
        return self.siril.Execute(commandes)
    
    def GetData(self):
        return self.siril.GetData()
       
    # --------------------------------------------------------------------------
    
    def filt_log(self, resultat_cmd):
        log=[]
        for ligne in resultat_cmd :
            if ligne[0:4] == 'log:' :
                log.append(ligne)
        return log
    
    def filt_status(self, resultat_cmd):
        status=[]
        for ligne in resultat_cmd :
            if ligne[0:7] == 'status:' :
                status.append(ligne)
        return status


    def pourcent_or_value(self, name_filt, filt_value ):  
        if filt_value == None :
            return "";   
        if type(filt_value) == str :
            return "-" + name_filt + "=" + filt_value + " "
        else:
            return "-" + name_filt + "=" + str(filt_value) + " "
            
    def str_filtering(self, filter_fwhm, filter_wfwhm, filter_round,filter_included):
        filtering = ""
        filtering +=self.pourcent_or_value("filter-fwhm",filter_fwhm)
        filtering +=self.pourcent_or_value("filter-wfwhm",filter_wfwhm)
        filtering +=self.pourcent_or_value("filter-round",filter_round)
        if filter_included==True :
            filtering += "-filter-included "
        return filtering 
        
    # --------------------------------------------------------------------------
    # wrapper des commandes
    
    ''' Non scriptable 
    def addmax(self,filename):
        """ addmax compute a new image IMG with IMG_1 and IMG_2. 
            The pixel of IMG_1 is replaced by the pixel at the same coordinates 
            of IMG_2 if the intensity of 2 is greater than 1. 
            Do not forget to save the result.
            
        Syntax     : status=app.addmax(filename)
        Parameters : Filename ( IMG_2 ) ( IMG_1 is the image loaded in memory)
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("addmax " + filename ))
    '''
    
    def asinh(self,stretch):
        """ asinh command stretches the image for show faint objects, while 
        simultaneously, preserve the structure of bright objects of the field.
            
        Syntax     : status=app.asinh(stretch)
        Parameters : stretch 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("asinh " + str(stretch) ))
    
    def bg(self):
        """ Returns the background level of the image loaded in memory.
            
        Syntax     : status=app.bg()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
            - status[1] = background level
        """
        status = self.Execute("bg" , bEndTest = True )
        if status is False :
            return (False)
        
        log = self.filt_log(self.GetData())
        if self.siril.bDev  :
            res = log[1].replace( ' ', '').split(':')[2].split('(')        
            return (status, int(res[0]), float(res[1][:-1]) )
        else:       
            res = log[1].replace( ' ', '').split(':')[-1] 
            return (status, res )
    
    def bgnoise(self):
        """ Returns the background noise level.
            
        Syntax     : status=app.bgnoise()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
            - status[1] = background noise level
        """
        status = self.Execute("bgnoise", bEndTest = True  )
        if status is False :
            return (False)
        
        log = self.filt_log(self.GetData())
        res_noise=[ ]
        for line in log : 
            if not re.match("^.*#",line) :
                continue
            res = re.sub( r"^.*#",'#', line )
            res=res.split(':')
            label=res[0][:-1].lower().replace('layer','').replace(')','')
            rr = res[1].split('(')
            res_noise.append( [ label.lower(), float(rr[0]), float(rr[1][:-1])] )
        return (status, res_noise )
            
    ''' Non scriptable 
    def boxselect(self,x=None,y=None, width=None, height=None):
        """ Make a selection with the arguments x, y, width and height.
            
        Syntax     : 
                    status=app.boxselect(x,y,width,height)
                    status=app.boxselect()
        Parameters : 
            - x,y      = coordinates of the image area
            - width    = width of the image area
            - height   = height of the image area
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
            - status[1] = ( x, y, width,height) if no parameter and a selection is active else None
        """
        status1=None
        if (x==None) or (y==None) or (width==None) or (height==None) :
            status = self.Execute("boxselect" )
            log = self.filt_log(self.GetData())
            rr = re.sub( r"^.*: ",'', log[0] )
            if rr != None :
                res=rr.split(' ')
                status1=(int(res[0]),int(res[1]),int(res[2]),int(res[3]))
                
        else:
            status = (self.Execute("boxselect "  + str(x) + ' ' + str(y) + ' ' + str(width) + ' ' + str(height) ))

        return ( status,status1)
    '''
    
    def cd(self,dirname):
        """ Set the new current working directory. 
        
        directory can contain the ~ token, expanded as the home directory, 
        directories with spaces in the name can be protected using single or 
        double quotes. Examples:
            cd ~/M42
            cd '../OIII 2x2/'
            
        Syntax     : status=app.cd(directory)
        Parameters : directory 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        os.chdir(dirname)
        return (self.Execute("cd '" + dirname.replace(os.sep,'/') + "'"))
    
    def cdg(self):
        """ Return the coordinates of the center of gravity of the image
            
        Syntax     : status=app.cdg()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
            - status[1] = x 
            - status[2] = y 
        """
        status = self.Execute("cdg", bEndTest = True  )
        if status is False :
            return (False)
        log = self.filt_log(self.GetData())
        res = log[1].replace( ' ', '').split('(')[1].split(',')        
        return (status, float(res[0]), float(res[1][:-1]) )
    
    def clahe(self, cliplimit, tileSize):
        """ Equalizes the histogram of an image using Contrast Limited Adaptive 
        Histogram Equalization.
            
        Syntax     : status=app.clahe(cliplimit, tileSize)
        Parameters : 
            - cliplimit sets the threshold for contrast limiting.
            - tilesize sets the size of grid for histogram equalization.
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """    
        return (self.Execute("clahe " + str(cliplimit) + ' ' + str(tileSize) ))
    
    ''' Non scriptable 
    def clear(self):
        """ Clears the graphical output logs
            
        Syntax     : status=app.clear()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("clear"))
    '''
    
    ''' Non scriptable     
    def clearstar(self):
        """ Clear all the stars saved in memory and displayed on the screen.
            
        Syntax     : status=app.clearstar()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("clearstar"))
    '''
    
    def close(self):
        """ Properly closes the opened image and the opened sequence, if any
            
        Syntax     : status=app.close()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("close"))
          
    def cosme(self,filename):
        """ cosmetic correction 
        
        Apply the local mean to a set of pixels on the in-memory image . 
        The coordinate of this pixels are in an ASCII file [list file]. COSME is 
        adapted to correct residual hot and cold pixels after preprocessing.
        The line P x y type will fix the pixel at coordinates (x, y) type is an 
        optional character (C or H) specifying to Siril if the current pixel is 
        cold or hot. This line is created by the command find_hot but you also 
        can add some lines manually.
        The line C x 0 type will fix the bad column at coordinates x.
        The line L y 0 type will fix the bad line at coordinates y.
            
        Syntax     : status=app.cosme(filename)
        Parameters : filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("cosme " + filename.replace(os.sep,'/') + " "))  # ne pas entourer de ' '
          
    def cosme_cfa(self,filename):
        """ cosmetic correction on CFA image
        
        Same function that COSME but applying to RAW CFA images.
            
        Syntax     : status=app.cosme_cfa(filename)
        Parameters : filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("cosme_cfa " + filename.replace(os.sep,'/') + " "))  # ne pas entourer de ' '
             
    def convert(self,basename, debayer=False, start=0, out='.', fitseq=False, ser=False):
        """ Convert all images in a known format into Siril's FITS images. 
        
        The argument "basename" is the basename of the new sequence. For FITS 
        images, Siril will try to make a symbolic link. If not possible, files 
        are copied. The option "-debayer" applies demosaicing to images, in this 
        case no symbolic link are done. "-start=index" sets the starting index 
        parameter and the "-out=" option allows to convert files into another 
        directory.
            
        Syntax     : status=app.convert(basename,[debayer=True] [start=index] [out=filename] [fitseq=True] [ser=True])
        Parameters : 
            - basename = new sequence basename
            - debayer  = set to True to debayerised
            - start    = starting index of new sequence
            - out      = output folder
            - fitseq   = saves as multi-image fit file
            - ser      = saves as ser file
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = basename+" " 
        if debayer == True :
            parameters+='-debayer '
        if start > 0 :
            parameters+= '-start=' + str(start) + ' '
        if out != '.' :
            parameters += "-out=" +  out.replace(os.sep,'/') + " " # ne pas entourer de ' '
        if fitseq == True :
            parameters+= '-fitseq '
        if ser == True :
            parameters+= '-ser '
        return (self.Execute("convert " + parameters ))
             
    def convertraw(self,basename, debayer=False, start=0, out='.', fitseq=False):
        """ Convert DSLR RAW files into Siril's FITS images or a FITS sequence (single image) 
        
        if "-fitseq" is provided. The basename argument is the base name of the 
        new  sequence. The debayer option applies demosaicing to images while 
        "-start=index"  sets the starting index parameter.
            
        Syntax     : status=app.convertraw(basename,[debayer=True] [start=index] [out=filename] [fitseq=True] [ser=True])
        Parameters : 
            - basename = new sequence basename
            - debayer  = set to True to debayerised
            - start    = starting index of new sequence
            - out      = output folder
            - fitseq   = saves as multi-image fit file
            - ser      = saves as ser file
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = basename+" " 
        if debayer == True :
            parameters+='-debayer '
        if start > 0 :
            parameters+= '-start=' + str(start) + ' '
        if out != '.' :
            parameters += "-out=" +  out.replace(os.sep,'/') + " "  # ne pas entourer de ' '
        if fitseq == True :
            parameters+= '-fitseq '
        return (self.Execute("convertraw " + parameters ))
    
    def crop(self,x,y, width, height):
        """ crop the image 
        It can be used with the GUI: if a selection has been made with the
        mouse, calling the crop command without arguments crops it on this 
        selection. Otherwise, or in scripts, arguments have to be given, with x 
        and y being the coordinates of the top left corner, and width and height 
        the size of the selection.
            
        Syntax     : status=app.crop(x, y, width, height)
        Parameters : 
            - x,y      = coordinates of the image area
            - width    = width of the image area
            - height   = height of the image area
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("crop " + str(x) + ' ' + str(y) + ' ' + str(width) + ' ' + str(height) ))
    
    ''' Non scriptable 
    def ddp(self,level, coef, sigma):
        """ Performs a DDP (digital development processing) 
        
        DDP as described first by Kunihiko Okano. This implementation is the 
        one described in IRIS. It combines a linear distribution on low levels 
        (below level) and a non-linear on high levels. It uses a Gaussian filter 
        of sigma multiplies the resulting image by coef. 
        The typical values for sigma are included between 0.7 and 2
            
        Syntax     : status=app.ddp(level, coef, sigma)
        Parameters : 
            - coef   
            - sigma   
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("ddp " + str(level) + ' ' + str(coef) + ' ' + str(sigma) ))
    '''
    
    def entropy(self):
        """ Computes the entropy of the opened image on the displayed layer, 
        only in the selected area if one has been selected or in the whole image 
        else. The entropy is one way of measuring the noise or the details in 
        an image.
            
        Syntax     : status=app.entropy()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
            - status[1] = entropy value
        """
        status = self.Execute("stat", bEndTest = True  )
        if status is False :
            return (False)
        
        log = self.filt_log(self.GetData())[1].split(':')
        return (status, float(log[-1]) )
    
    def exit(self):
        """ Quits the application.
            
        Syntax     : status=app.exit()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("exit"))
      
    def extract(self,NbPlane):
        """ Extracts NbPlane Planes of Wavelet domain
            
        Syntax     : status=app.extract(NbPlane)
        Parameters : 
            - NbPlane :  plane number
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("extract " + str(NbPlane) ))
        
    def extract_Green(self):
        """ Extracts green signal from the currently loaded CFA image. It reads 
        the Bayer matrix information from the image or the preferences and exports 
        only the averaged green filter data as a new half-sized FITS file. 
        The output file name starts with the prefix "Green_".
            
        Syntax     : status=app.extract_Green()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("extract_Green" ))
        
        
    def extract_Ha(self):
        """Extracts H-alpha signal from the currently loaded CFA image. It reads 
        the Bayer matrix information from the image or the preferences and exports 
        only the red filter data as a new half-sized FITS file. The output file 
        name starts with the prefix "Ha_". 
            
        Syntax     : status=app.extract_Ha()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("extract_Ha" ))
        
    def extract_HaOIII(self):
        """ Extracts H-alpha and O-III signals from the currently loaded CFA image. 
        It reads the Bayer matrix information from the image or the preferences 
        and exports only the red filter data for H-alpha and an average of the 
        three others as a new half-sized FITS files. The output file name start 
        with the prefixes "Ha_" and "OIII_".
            
        Syntax     : status=app.extract_HaOIII()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("extract_HaOIII" ))
    
    def fdiv(self,filename, scalar):
        """ Divides the image in memory by the image given in argument. The 
        resulting image is multiplied by the value of the scalar argument. 
        Please check that the image is in the working directory. See also idiv.
            
        Syntax     : status=app.fdiv(filename, scalar)
        Parameters : 
            - filename
            - scalar
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("fdiv " + filename.replace(os.sep,'/') + " " + str(scalar)  ))  # ne pas entourer de ' '
    
    def fftd(self,modulus, phase):
        """ Applies a Fast Fourier Transform to the image loaded in memory. 
        Modulus and phase given in argument are saved in FITS files.
            
        Syntax     : status=app.fftd(modulus, phase)
        Parameters : 
            - modulus
            - phase
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("fftd " + str(modulus) + ' ' + str(phase)  ))
    
    def ffti(self,modulus, phase):
        """ This function is used to retrieve corrected image applying an 
        inverse transformation. The modulus and phase used are the files given 
        in argument.
            
        Syntax     : status=app.ffti(modulus, phase)
        Parameters : 
            - modulus
            - phase
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("ffti " + str(modulus) + ' ' + str(phase)  ))
    
    def fill(self,value,x=None,y=None, width=None, height=None):
        """ Fills the whole current image (or selection) with pixels having the 
        value intensity.
            
        Syntax     : status=app.fill(value, x, y, width, height)
        Parameters : 
            - x,y      = coordinates of the image area
            - width    = width of the image area
            - height   = height of the image area
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        if (x==None) or (y==None) or (width==None) or (height==None) :
            return (self.Execute("fill " + str(value) )  )      
        else:
            return (self.Execute("fill " + str(value) + ' '  + str(x) + ' ' + str(y) + ' ' + str(width) + ' ' + str(height) ))
    
    def fill2(self,value,x,y, width, height):
        """ Same command than FILL but this is a symmetric fill of a region 
        defined by the mouse. Used to process an image in the Fourier (FFT) domain.
            
        Syntax     : status=app.fill2(value, x, y, width, height)
        Parameters : 
            - x,y      = coordinates of the image area
            - width    = width of the image area
            - height   = height of the image area
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("fill2 " + str(value) + ' '  + str(x) + ' ' + str(y) + ' ' + str(width) + ' ' + str(height) ))
    
    def find_cosme(self,cold_sigma, hot_sigma):
        """ This command applies an automatic detection of cold and hot pixels 
        following the threshold written in arguments.
            
        Syntax     : status=app.find_cosme(cold_sigma, hot_sigma)
        Parameters : 
            - cold_sigma = threshold of cold pixels
            - hot_sigma  = threshold of hot pixels
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("find_cosme " + str(cold_sigma) + ' ' + str(hot_sigma)  ))
    
    def find_cosme_cfa(self,cold_sigma, hot_sigma):
        """ Same command than FIND_COSME but for monochromatic CFA images.
            
        Syntax     : status=app.find_cosme_cfa(cold_sigma, hot_sigma)
        Parameters : 
            - cold_sigma = threshold of cold pixels
            - hot_sigma  = threshold of hot pixels
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("find_cosme_cfa " + str(cold_sigma) + ' ' + str(hot_sigma)  ))
    
    def find_hot(self, filename, cold_sigma, hot_sigma):
        """ The command provides a list file "filename" (format text) in the 
        working directory which contains the coordinates of the pixels which 
        have an intensity "hot_sigma" times higher and "cold_sigma" lower than 
        standard deviation. We generally use this command on a master-dark file.
            
        Syntax     : status=app.find_hot(filename,cold_sigma, hot_sigma)
        Parameters : 
            - filename 
            - cold_sigma = threshold of cold pixels
            - hot_sigma  = threshold of hot pixels
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("find_hot " + filename.replace(os.sep,'/') + " " + str(cold_sigma) + ' ' + str(hot_sigma)  ))  # ne pas entourer de ' '
    
    ''' Non scriptable 
    def findstar(self):
        """ Detects stars having a level greater than a threshold computed by 
        Siril. The algorithm is based on the publication of Mighell, K. J. 1999, 
        in ASP Conf. Ser., Vol. 172, Astronomical Data Analysis Software and 
        Systems VIII, eds. D. M. Mehringer, R. L. Plante, & D. A. Roberts (San 
        Francisco: ASP), 317.
            
        Syntax     : status=app.findstar()
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("findstar"))
    '''
    
    def fix_xtrans(self):
        """ Fixes the Fujifilm X-Trans Auto Focus pixels. Indeed, because of 
        the phase detection auto focus system, the photosites used for auto focus 
        get a little less light than the surrounding photosites. The camera 
        compensates for this and increases the values from these specific 
        photosites giving a visible square in the middle of the dark/bias frames. 
            
        Syntax     : status=app.fix_xtrans()
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("fix_xtrans"))
    
    def fixbanding (self,amount , sigma):
        """ Try to remove the canon banding. Argument "amount" define the amount 
        of correction. Sigma defines a protection level of the algorithm, higher 
        sigma gives higher protection.
            
        Syntax     : status=fixbanding(amount, sigma)
        Parameters : 
            - amount
            - sigma
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("fixbanding " + str(amount) + ' ' + str(sigma)  ))
    
    def fmedian(self,ksize, modulation):
        """ Performs a median filter of size ksize x ksize (ksize MUST be odd) 
        to the original image with a modulation parameter . 
        The output pixel is computed as : out =mod × m+(1−mod)×in, where m is 
        the median-filtered pixel value. A modulation's value of 1 will apply 
        no modulation.
            
        Syntax     : status=app.fmedian(ksize, modulation)
        Parameters : 
            - ksize
            - modulation  
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("fmedian " + str(ksize) + ' ' + str(modulation)  ))
    
    def fmul(self,scalar):
        """ Multiplies the loaded image by the scalar given in argument.
            
        Syntax     : status=app.fmul(scalar)
        Parameters : 
            - scalar
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("fmul " + str(scalar)  ))
    
    def gauss(self,sigma):
        """ Performs a Gaussian filter with the given sigma.
            
        Syntax     : status=app.gauss(sigma)
        Parameters : 
            - sigma
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("gauss " + str(sigma)  ))
      
    def grey_flat(self):
        """ The function equalizes the mean intensity of RGB layers in a CFA images.
            
        Syntax     : status=app.grey_flat()
        Parameters : None  
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("grey_flat " + str(NbPlane) ))
    
    def help(self, name=None):
        """ help on SiriL commands.
            
        Syntax     : status=app.help( name )
                     status=app.help(  )
        Parameters : 
            - name : command name
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        str=help.search(name)
        print(str)
    
    def histo(self,layer):
        """ Calculates the histogram of the image layer in memory and produces 
        file histo_[layer name].dat in the working directory.
            
        Syntax     : status=app.histo( layer )
        Parameters : 
            - layer : 0, 1 or 2 with 0=red, 1=green and 2=blue.
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("histo " + str(layer)  ))
    
    def iadd(self,filename):
        """ Adds the image in memory to the image designed in argument. Please 
        check that the image is in the working directory.
            
        Syntax     : status=app.iadd(filename)
        Parameters : 
            - filename : image to add
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("iadd  " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    def idiv(self,filename):
        """ Divides the image in memory by the image given in argument. Please 
        check that the image is in the working directory. See also fdiv.
            
        Syntax     : status=app.idiv(filename)
        Parameters : 
            - filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("idiv  " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    def imul(self,filename):
        """ Multiplies the image in memory by the image given in argument. Please 
        check that the image is in the working directory. See also fdiv.
            
        Syntax     : status=app.imul(filename)
        Parameters : 
            - filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("imul  " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    def isub(self,filename):
        """ Substracts the image in memory by the image designed in argument. Please 
        check that the image is in the working directory.
            
        Syntax     : status=app.isub(filename)
        Parameters : 
            - filename : image to substract
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("isub  " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    def linear_match(self, reference, low, high):
        """ Computes a linear function between a reference image and a target. 
        The function is then applied to the target image to match it to the 
        reference one. The algorithm will ignore all reference pixels whose 
        values are outside of the [low, high] range.
            
        Syntax     : status=app.linear_match(reference, low, high)
        Parameters : 
            -reference : reference image
            - low      : low threshold
            - high     : high threshold
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("linear_match " + reference.replace(os.sep,'/') + ' '  + str(low) + ' ' + str(high) ))
             
    def link(self,basename, start=0,out='.'):
        """ Link all FITS images in the working directory with the basename given 
        in argument. If no symbolic link could be created, files are copied.
        It is possible to convert files in another directory with the "out" option.
            
        Syntax     : status=app.link(basename, [start=index])
        Parameters : 
            - basename : output basename
            - start    : index of the first image
            - out      :  output directory
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = " " 
        if start > 0 :
            parameters+= '-start=' + str(start) + ' '
        if out != '.' :
            parameters += "-out=" +  out.replace(os.sep,'/') + " " # ne pas entourer de ' '
        return (self.Execute("link " + basename + " " + parameters ))
    
    def load(self,filename):
        """ Loads the image filename; it first attempts to load filename, then 
        filename.fit and finally filename.fits and after, all supported format, 
        aborting if none of these are found. These scheme is applicable to every 
        Siril command implying reading files. Fits headers MIPS-HI and MIPS-LO 
        are read and their values given to the current viewing levels. Writing a 
        known extension at the end of filename will load the image filename.ext: 
        this is used when numerous files have the same name but not the same 
        extension.

        Extensions supported are :
        
            *.fit, *.fits, *.fts
            *.bmp / *.jpg, *.jpeg / *.png / *.tif, *.tiff
            *.ppm, *.pgm
            *.pic (IRIS file)
            
        Syntax     : status=app.load(filename)
        Parameters : 
            - filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("load " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    def log(self):
        """ Computes and applies a logarithmic scale to the current image.
            
        Syntax     : status=app.log()
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("log"))
    
    def ls(self):
        """ list files of work folder
            
        Syntax     : status=app.ls()
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        if self.siril.bDev  :
            return (self.Execute("ls"))
        else:
            return (False)

    def merge(self, list_seq, new_seq ):
        """ Merges several sequences of the same type (FITS images, FITS sequence 
        or SER) and same image properties into a new sequence with base name 
        newseq created in the current working directory, with the same type. 
        The input sequences can be in different directories, can specified either 
        in absolute or relative path, with the exact .seq name or with only 
        the base name with or without the trailing '_'. 
            
        Syntax     : status=app.merge(list_seq, new_seq )
        Parameters : 
            - list_seq : list of sequences to merge
            - new_seq : the new sequence which merge all files
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        seq_to_merge=""
        for seq in list_seq :
            seq_to_merge = seq_to_merge + " " + seq
            
        return (self.Execute("merge " + seq_to_merge + " " + new_seq ))
    
    def mirrorx(self):
        """ Rotates the image around a vertical axis.
            
        Syntax     : status=app.mirrorx()
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("mirrorx"))
    
    def mirrory(self):
        """ Rotates the image around a horizontal axis.
            
        Syntax     : status=app.mirrory()
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("mirrory"))
    
    def mtf(self, low, midtone , high):
        """ Applies midtone transfer function to the current loaded image.
            
        Syntax     : status=app.mtf( low, midtone , high)
        Parameters : 
            - low 
            - midtone
            - high
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("mtf " + str(low) + ' ' + str(midtone) + ' '  + str(high) ))
        
    def neg(self):
        """ Shows the negative view of the current image.
            
        Syntax     : status=app.neg()
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("neg"))
    
    ''' Non scriptable 
    def new(self, width, height , nb_layers):
        """ Creates a new image filled with zeros with a size of width x height. 
        The image is in 16-bit format, and it contains nb_layers layers, nb_layers 
        being 1 or 3. It is not saved, but displayed and can be saved afterwards.
            
        Syntax     : status=app.new( width, height , nb_layers)
        Parameters : 
            - width     : image width
            - height    : image height
            - nb_layers : layers number
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("new " + str(width) + ' ' + str(height) + ' '  + str(nb_layers) ))
    '''
    
    def nozero(self, level):
        """ Replaces null values by level values. Useful before an idiv or fdiv 
        operation.
            
        Syntax     : status=app.nozero( level )
        Parameters : 
            - level : new value of null pixel
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("nozero " + str(level) ))
    
    def offset(self, value):
        """ Adds the constant value to the current image. This constant can take 
        a negative value. As Siril uses unsigned fit files, if the intensity of 
        the pixel become negative its value is replaced by 0 and by 65535 
        (for a 16-bit file) if the pixel intensity overflows. To check the 
        minimum and maximum intensities values, click on the Auto level button 
        and note the low and high threshold.
            
        Syntax     : status=app.offset( level )
        Parameters : 
            - value : offset value
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("offset " + str(value) ))

    def preprocess(self,sequencename, bias=None, dark=None,flat=None, 
                   cfa=False,debayer=False, fix_xtrans=False, equalize_cfa=False, 
                   opt=False, prefix=None, fitseq=False ):
        """ Preprocesses the sequence "sequencename" using bias, dark and flat 
        given in argument. It is possible to specify if images are CFA for 
        cosmetic correction purposes with the option "-cfa" and also to demosaic 
        images at the end of the process with "-debayer". The "-fix_xtrans" 
        option is dedicated to X-Trans files by applying a correction on darks 
        and biases to remove an ugly square pattern and the "-equalize_cfa" 
        option equalizes the mean intensity of RGB layers of the CFA flat master. 
        It is also possible to optimize the dark subtraction with "-opt". The 
        output sequence name starts with the prefix "pp_" unless otherwise 
        specified with option "-prefix=". If "-fitseq" is provided, the output 
        sequence will be a FITS sequence (single file).
        Note that only hot pixels are corrected in cosmetic correction process
            
        Syntax     : status=app.preprocess(sequencename, [bias=filename], [dark=filename], [flat=filename], [cfa], [debayer=True], [fix_xtrans=True], [equalize_cfa=True], [opt=True], [-prefix=xxx] [fitseq=True])
        Parameters : 
            - sequencename : basename of input images
            - bias         : master-bias filename
            - dark         : master-dark filename
            - flat         : master-flat filename
            - cfa          : the images are CFA type
            - debaye       : debayerised
            - fix_xtrans   : option to  X-Trans files
            - equalize_cfa : option to equalize CFA images 
            - opt          : to optimize the dark subtraction
            - prefix       : set the prefix of output images
            - fitseq       : save in a multi-image FIT file 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = sequencename + " " 
        if bias != None :
            parameters+="-bias=" + bias.replace(os.sep,'/') + " "  # ne pas entourer de ' '
        if dark != None :
            parameters+="-dark=" + dark.replace(os.sep,'/') + " "  # ne pas entourer de ' '
        if flat != None :
            parameters+="-flat=" + flat.replace(os.sep,'/') + " "  # ne pas entourer de ' '
            
        if cfa == True :
            parameters+='-cfa '            
        if debayer == True :
            parameters+='-debayer '
        if fix_xtrans == True :
            parameters+= '-fix_xtrans '
        if equalize_cfa == True :
            parameters+='-equalize_cfa '            
        if opt == True :
            parameters+='-opt '            
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/') + " "          
        if fitseq == True :
            parameters+= '-fitseq '
        return (self.Execute("preprocess " + parameters ))

    ''' Non scriptable 
    def psf(self):
        """ Performs a PSF (Point Spread Function) on the selected star.
        Make a selection around a star and call the command PSF. It will give you:

            The centroid coordinates (x0 and y0) in pixel units, which is the position of the center of symmetry of the fitted PSF.
            The FWHM on the X and Y axis.
            The rotation angle of the X axis with respect to the centroid coordinates.
            The average local background.
            The maximal intensity of the star: this is the peak value of the fitted function, located at the centroid coordinates x0 and y0.
            The relative magnitude of the star.
            The RMSE. This is an estimate of fitting quality. The smaller the RMSE is, the better the function is fitted.
        
        To be relevant, the selection MUST be done on a non-saturated star.
            
        Syntax     : status=app.psf( level )
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        self.tr.warning("psf() :" +  _("not yet implemented"))
        return ( False )
    '''
    
    def register(self,sequencename, norot=False, drizzle=False, prefix=None ):
        """ Performs geometric transforms on images of the sequence given in 
        argument so that they may be superimposed on the reference image. The 
        output sequence name will start with the prefix "r_" unless otherwise 
        specified with "-prefix=" option. Using stars for registration, this 
        algorithm only works with deepsky images. The registration is done on 
        the green layer for RGB images. It may work with CFA (not demosaiced) 
        images, but because of luminance factors between each filter and the 
        star colours, it will not be precise and therefore not recommended.
        The option "-norot" performs a translation only registration with no new 
        sequence built. The option "-drizzle" activates the sub-pixel stacking, 
        either by up-scaling by 2 the images created in the rotated sequence or 
        by setting a flag that will proceed to the up-scaling during stacking if 
        "-norot" is passed.
            
        Syntax     : status=app.register(sequence, [norot=True] [drizzle=True] [prefix=xxx])
        Parameters : 
            - sequence : basename of input images
            - norot    : performs a translation only registration
            - drizzle  : up-scaling by 2
            - prefix   : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = sequencename + " " 
        if norot == True :
            parameters+='-norot '            
        if drizzle == True :
            parameters+='-drizzle '
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')            
        return (self.Execute("register " + parameters ))
    
    def requires(self, version_str):
        """ This function returns an error if the version of Siril is older that the one passed in argument.
            
        Syntax     : status=app.requires(version_str)
        Parameters : 
            - version_str : format x.y.z ( ie: "0.99.6" )
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("requires " + version_str ))
    
    def resample(self, factor):
        """ Resamples image with a factor "factor".
            
        Syntax     : status=app.resample(factor)
        Parameters : 
            - factor 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("resample " + str(factor) ))
    
    def rgradient(self, xc, yc, dR, dalpha):
        """ Creates two images, with a radial shift ("dR" in pixels) and a 
        rotational shift ("dalpha" in degrees) with respect to the point 
        ("xc", "yc"). Between these two images, the shifts have the same 
        amplitude, but an opposite sign. The two images are then added to create 
        the final image. This process is also called Larson Sekanina filter.
            
        Syntax     : status=app.rgradient(xc, yc, dR, dalpha)
        Parameters : 
            - xc , yc : points
            - dR      : radial shift
            - dalpha  : rotational shift in degrees
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("rgradient " + str(xc) + ' ' + str(yc) + ' ' + str(dR) + ' ' +  str(dalpha)))
    
    def rl(self, threshold, sigma, corner_radius_boost, iterations):
        """ Restores an image using the Richardson-Lucy method. Threshold 
        generates an internal mask based on microcontrast in the image which then 
        is used to blend the deconvoluted with the convoluted image. Sigma is 
        the size of the kernel to be applied, while corner_radius_boost is a 
        value which is added to Gaussian sigma for the tiles in the corners of 
        an image. Iterations is the number of iterations to be performed.
            
        Syntax     : status=app.rl(threshold, sigma, corner_radius_boost, iterations)
        Parameters : 
            - threshold
            - sigma
            - corner_radius_boost
            - iterations
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("rl " + str(threshold) + ' ' + str(sigma) + ' ' + str(corner_radius_boost) + ' ' +  str(iterations)))
    
    def rmgreen(self, type):
        """ rmgreen is a chromatic noise reduction filter. It removes green 
        noise in the current image. This filter is based on PixInsight's SCNR 
        Average Neutral algorithm and it is the same filter used by HLVG plugin 
        in Photoshop. In command line, the lightness is always preserved. For 
        image processing without L* preservation use the graphical tool box and 
        uncheck the corresponding button.
            
        Syntax     : status=app.rmgreen(type)
        Parameters : 
            - type :
                o 1 stands for Average Neutral Protection
                o 2 stands for Maximum Neutral Protection
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("rmgreen " + str(type) ))
    
    def rotate(self, degree, nocrop=False):
        """ Rotates the image of an angle of degree value.
            
        Syntax     : status=app.rotate(degree, [nocrop=True] )
        Parameters : 
            - degree : rotation angle in degree
            - nocrop : option can be added to avoid the cropping
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = str(degree) + " " 
        if nocrop == True :
            parameters+='-nocrop '            
        return (self.Execute("rotate " + parameters ))

    def rotatepi(self):
        """ Rotates the image of an angle of 180° around its center. This is 
        equivalent to the command "rotate 180" or "rotate -180"
            
        Syntax     : status=app.rotatepi()
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("rotatepi"))
    
    def satu(self, coeff):
        """ Enhances the global saturation of the image. 
        Try iteratively to obtain best results. For example: satu 0.1
            
        Syntax     : status=app.satu(coeff)
        Parameters : 
            - coeff 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("satu " + str(coeff) ))
    
    def save(self,filename):
        """ Saves current image to filename.fit. Fits headers MIPS-HI and MIPS-LO 
        are added with values corresponding to the current viewing levels.
            
        Syntax     : status=app.save(filename)
        Parameters : 
            - filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("save " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    def savebmp(self,filename):
        """ Saves current image under the form of a bitmap file with 8bits per 
        channel: filename.bmp (BMP 24 bits).
            
        Syntax     : status=app.savebmp(filename)
        Parameters : 
            - filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("savebmp " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    def savejpg(self,filename,quality=100):
        """ Saves current image into a JPG file. You have the possibility to 
        adjust the quality of the compression. A value 100 for quality parameter 
        offers best fidelity while a low value increases the compression ratio. 
        If no value is specified, it holds a value of 100. This command is very 
        usefull to share an image in the jpeg format on the forums for example.
            
        Syntax     : status=app.savejpg(filename, [quality=100])
        Parameters : 
            - filename 
            - quality
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("savejpg " + filename.replace(os.sep,'/') + "  " + str(quality) ))  # ne pas entourer de ' '
    
    def savepng(self,filename):
        """ Saves current image as a PNG file.
            
        Syntax     : status=app.savepng(filename)
        Parameters : 
            - filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("savepng " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    def savepnm(self,filename):
        """ Saves current image under the form of a Netpbm file format with 
        16bits per channel. The extension of the output will be filename.ppm for 
        RGB image and filename.pgm for gray-level image. More details about the 
        Netpbm format at : http://en.wikipedia.org/wiki/Netpbm_format.
            
        Syntax     : status=app.savepnm(filename)
        Parameters : 
            - filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("savepnm " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    def savetif(self,filename):
        """ Saves current image under the form of a uncompressed TIFF file with 
        16bits per channel.
            
        Syntax     : status=app.savetif(filename)
        Parameters : 
            - filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("savetif " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    def savetif32(self,filename):
        """ Same command than SAVE_TIF but the output file is saved in 32bits 
        per channel.
            
        Syntax     : status=app.savetif32(filename)
        Parameters : 
            - filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("savetif32 " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    def savetif8(self,filename):
        """ Same command than SAVE_TIF but the output file is saved in 8bits 
        per channel.
            
        Syntax     : status=app.savetif(filename)
        Parameters : 
            - filename 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("savetif8 " + filename.replace(os.sep,'/') + " " ))  # ne pas entourer de ' '
    
    ''' Non scriptable 
    def select(self,first,last):
        """ This command allows easy mass selection of images in the loaded 
        sequence (first - last, to included). Examples:
            - selects the first.
                select 0 0
            - selects 201 images starting from number 1000
                select 1000 1200
        The second number can be greater than the number of images to just go up 
        to the end.
        
        See UNSELECT.
           
        Syntax     : status=app.savetif(first, last)
        Parameters : 
            - first index
            - last index 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("select " + first + " " + last))
    '''
    
    def seqcrop(self, prefix=None ):
        """ Crops the loaded sequence. The output sequence name starts with the 
        prefix "cropped_" unless otherwise specified with "-prefix=" option.
                
        Syntax     : status=app.seqcrop(prefix)
        Parameters : 
            - prefix : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = " " 
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')            
        return (self.Execute("seqcrop " + parameters ))

    def seqextract_Green (self, seqname, prefix=None):
        """ Same command than extract_Green but for the sequence "seqname". 
        The output sequence name starts with the prefix "Green_" unless otherwise 
        specified with option "-prefix=".
                
        Syntax     : status=app.seqextract_Green(seqname,prefix)
        Parameters : 
            - sequence : basename of input images
            - prefix   : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = " " 
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')   
        return (self.Execute("seqextract_Green " + seqname + ' ' + parameters))

    def seqextract_Ha (self, seqname, prefix=None):
        """ Same command than extract_Ha but for the sequence "seqname". 
        The output sequence name starts with the prefix "Ha_" unless otherwise 
        specified with option "-prefix=".
                
        Syntax     : status=app.seqextract_Ha(seqname,prefix)
        Parameters : 
            - sequence : basename of input images
            - prefix   : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = " " 
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')   
        return (self.Execute("seqextract_Ha " + seqname + ' ' + parameters))

    def seqextract_HaOIII  (self, seqname, prefix=None ):
        """ Same command than EXTRACT_HAOIII but for the sequence "seqname". The 
        output sequence name start with the prefix "Ha_" and "OIII_".
                
        Syntax     : status=app.seqextract_HaOIII(seqname,prefix)
        Parameters : 
            - sequence : basename of input images
            - prefix   : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = " " 
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')   
        return (self.Execute("seqextract_HaOIII  " + seqname + ' ' + parameters ))

    def seqcosme(self, seqname, filename=None, prefix=None ):
        """ Same command as COSME but for the the sequence sequencename.
                
        Syntax     : status=app.seqcosme(cold_sigma, hot_sigma, [prefix=xxx])
        Parameters : 
            - seqname    : sequence name
            - filename   : hot threshold
            - prefix     : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = seqname + " "   
        if filename != None :
            parameters+=  filename.replace(os.sep,'/')   +  " "          
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')            
        return (self.Execute("seqcosme " +  parameters ))

    def seqcosme_cfa(self, seqname, filename=None, prefix=None ):
        """ Same command as COSME_CFA but for the the sequence sequencename.
                
        Syntax     : status=app.seqcosme_cfa(cold_sigma, hot_sigma, [prefix=xxx])
        Parameters : 
            - seqname    : sequence name
            - filename   : hot threshold
            - prefix     : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = seqname + " "   
        if filename != None :
            parameters+=  filename.replace(os.sep,'/')   +  " "          
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')            
        return (self.Execute("seqcosme_cfa " +  parameters ))

    def seqfind_cosme(self, seqname, cold_sigma, hot_sigma, prefix=None ):
        """ Same command than FIND_COSME but for the loaded sequence. The output 
        sequence name starts with the prefix "cc_" unless otherwise specified 
        with "-prefix=" option
                
        Syntax     : status=app.seqfind_cosme(cold_sigma, hot_sigma, [prefix=xxx])
        Parameters : 
            - cold_sigma : cold threshold
            - hot_sigma  : hot threshold
            - prefix     : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = seqname + " " + str(cold_sigma)  + " " + str(hot_sigma) + " "
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')            
        return (self.Execute("seqfind_cosme  " + parameters ))

    def seqfind_cosme_cfa(self, seqname, cold_sigma, hot_sigma, prefix=None ):
        """ Same command than FIND_COSME_CFA but for the loaded sequence. The 
        output sequence name starts with the prefix "cc_" unless otherwise 
        specified with "-prefix=" option.
                
        Syntax     : status=app.seqfind_cosme_cfa(cold_sigma, hot_sigma, [prefix=xxx])
        Parameters : 
            - cold_sigma : cold threshold
            - hot_sigma  : hot threshold
            - prefix     : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = seqname + " " + str(cold_sigma)  + " " + str(hot_sigma) + " "
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')            
        return (self.Execute("seqfind_cosme_cfa  " + parameters ))

    def seqmtf(self, seqname, low, midtone, high, prefix=None ):
        """ Same command than MTF but for the sequence seqname. The output 
        sequence name starts with the prefix "mtf_" unless otherwise specified 
        with "-prefix=" option.
                
        Syntax     : status=app.seqmtf(seqname, low, midtone, high, [prefix=xxx])
        Parameters : 
            - sequence   : basename of input images
            - cold_sigma : cold threshold
            - hot_sigma  : hot threshold
            - prefix     : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = " " 
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')            
        return (self.Execute("seqmtf " + seqname + " " + str(low)  + " " + str(midtone) + " "+ str(high) + " "  + parameters ))

    ''' Non scriptable 
    def seqpsf(self):
        """ Same command than PSF but works for sequences. 
        Results are dumped in the console in a form that can be used to produce 
        brightness variation curves.
            
        Syntax     : status=app.seqpsf( level )
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        self.tr.warning("seqpsf() :" +  _("not yet implemented"))
        return ( False )
    '''
    
    def seqsplit_cfa(self, seqname, prefix=None ):
        """ Same command that SPLIT_CFA but for the sequence sequencename. The 
        output sequence name starts with the prefix "CFA_" unless otherwise 
        specified with "-prefix=" option.
            
        Syntax     : status=app.seqsplit_cfa( seqname, [prefix=xxx])
        Parameters : 
            - sequence   : basename of input images
            - prefix     : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = " " 
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')            
        return (self.Execute("seqsplit_cfa " + seqname  + " "  + parameters ))

    def seqsubsky(self, seqname, degree, prefix=None ):
        """ Same command that SUBSKY but for the sequence sequencename. The 
        output sequence name starts with the prefix "bkg_" unless otherwise 
        specified with "-prefix=" option.
            
        Syntax     : status=app.seqsubsky( seqname, degree, [prefix=xxx])
        Parameters : 
            - sequence   : basename of input images
            - degree     : order degree
            - prefix     : set the prefix of output images
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        parameters = " " 
        if prefix != None :
            parameters+="-prefix=" + prefix.replace(os.sep,'/')            
        return (self.Execute("seqsubsky " + seqname  + " " + str(degree) + " "  + parameters ))
    
    def seqstat(self, seqname, output_csv="statistic.csv" , option = "basic" ):
        """ Returns global statistic of the images sequence
        
        syntax     : status=app.seqstat( sequencename, output_csv , option )
        Parameters : 
            - sequencename : basename of input images
            - output_csv   : csv output filename
            - option       : basic or main 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        
        """     
        return (self.Execute("seqstat " + seqname  + " " + output_csv + " "  + option ))
        
    
    def setref(self, seqname, image):
        """ Sets the reference image of the sequence given in first argument. 
            
        Syntax     : status=app.setref( seqname, image )
        Parameters : 
            - sequencename : basename of input images
            - image        : reference image. 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("setref " + seqname  + " "  + str(image) ))
    
    def set16bits(self):
        """ Disallow images to be saved with 32 bits per channel on processing, 
        use 16 instead
            
        Syntax     : status=app.set16bits( )
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("set16bits"))
    
    def set32bits(self):
        """ Allow images to be saved with 32 bits per channel on processing.
            
        Syntax     : status=app.set32bits( )
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("set32bits"))

    def setcompress(self, enable, type=None, quantif=16 ):
        """ Defines if images are compressed or not: 0 means no compression. If 
        compression is enabled, the type must be explicitly written in the 
        option "-type=" ("rice", "gzip1", "gzip2"). Associated  to the compression, 
        the quantization value must follow [0, 256] . 
        
        For example, "setcompress 1 -type=rice 16" set the rice compression with 
        a quantization of 16.
            
        Syntax     : status=app.setcompress(enable, [type], [quantif], [hscale_factor] ))
        Parameters : 
            - enable   : set or unset the compression
            - type     : set the compression type ("rice", "gzip1", "gzip2")
            - quantif  : quantization value ([0, 256])
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        
        if not enable  :
             return (self.Execute("setcompress 0" ))
        parameters = " " 
        if type == None :
            return (False )
        if (type == "rice") or (type == "gzip1") or (type == "gzip2") :
            parameters += "-type=" + type + " "    
        else:
            return (False )
        parameters +=  quantif                  
        return (self.Execute("setcompress 1 " +  parameters ))
    
    def setcpu(self, number):
        """ Defines the number of processing threads used for calculation. Can 
        be as high as the number of virtual threads existing on the system, 
        which is the number of CPU cores or twice this number if hyper-threading 
        (Intel HT) is available. The default value is the maximum number of 
        threads available, so this should mostly be used to limit processing 
        power. See also SETMEM
            
        Syntax     : status=app.setcpu( number )
        Parameters : 
            - number: set the number of cpu used
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("setcpu " + str(number)))
    
    def setext(self, ext):
        """ Sets the extension used and recognized by sequences. The argument 
        "extension" can be "fit", "fts" or "fits".
            
        Syntax     : status=app.setext( ext )
        Parameters : 
            - ext : set the extension value ( "fit", "fts" or "fits" )
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("setext " + ext ))
    
    def setfindstar(self, ksigma, roundness):
        """ Defines thresholds above the noise and star roundness for all 
        subsequent stars detection with FINDSTAR and REGISTER commands of the 
        session. Ksigma must be greater or equal to 0.05 and roundness between 
        0 and 0.9.

        The threshold for star detection is computed as the median of the image 
        (which represents in general the background level) plus Ksigma times 
        sigma, sigma being the standard deviation of the image (which is a good 
        indication of the noise amplitude). If you have many stars in your 
        images and a good signal/noise ratio,  it may be a good idea to increase 
        this value to speed-up the detection and false positives.

        The roundness argument is the minimal ratio of the short axis on the 
        long axis of the star Gaussian fit (see PSF). A value of 1 would keep 
        only perfectly round stars, a value of 0.5, the default, means that 
        stars detected twice as big on an axis as on the other will still be 
        used for registration.

        It is recommended to test the values used for a sequence with 
        Siril's GUI, available in the dynamic PSF toolbox from the analysis menu. 
        It may improve registration quality to increase the parameters, but it 
        is also important to be able to detect several tens of stars in each image.
            
        Syntax     : status=app.setfindstar(ksigma, roundness)
        Parameters : 
            - ksigma    : coefficient >= 0.05
            - roundness : roundness between [0, 0.9]
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("setfindstar " + str(ksigma) + ' ' + str(roundness) ))
    
    ''' Non scriptable 
    def setmag(self, magnitude):
        """ Calibrates the magnitude by selecting a star and giving the known 
        apparent magnitude. All PSF computations will return the calibrated 
        apparent magnitude afterwards, instead of an apparent magnitude relative 
        to ADU values. To reset the magnitude constant see UNSETMAG.
            
        Syntax     : status=app.setmag( magnitude )
        Parameters : 
            - magnitude : known apparent magnitude of a selected star
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("setmag " + str(magnitude)))
    '''
    
    ''' Non scriptable 
    def setmagseq(self, magnitude):
        """ This command is only valid after having run seqpsf or its graphical 
        counterpart (select the area around a star and launch the psf analysis 
        for the sequence, it will appear in the graphs). This command has the 
        same goal as setmag but recomputes the reference magnitude for each 
        image of the sequence where the reference star has been found. When 
        running the command, the last star that has been analysed will be 
        considered as the reference star. Displaying the magnitude plot before 
        typing the command makes it easy to understand. To reset the reference 
        star and magnitude offset, see unsetmagseq.
            
        Syntax     : status=app.setmagseq( magnitude )
        Parameters : 
            - magnitude : known apparent magnitude  of a reference star
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("setmagseq " + str(magnitude)))
    '''
    
    def setmem(self, ratio):
        """ Sets a new ratio of free memory on memory used for stacking. Value 
        should be between 0 and 1, depending on other activities of the machine. 
        A higher ratio should allow siril to stack faster. Introduced in 0.9.11. 
        See also SETCPU
            
        Syntax     : status=app.setmem( ratio )
        Parameters : 
            - ratio : ratio of used memory
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("setmem " + str(ratio)))
    
    def split(self, rfile, gfile, bfile):
        """ Splits the color image into three distincts files (one for each 
        color) and save them in r g and b file.
            
        Syntax     : status=app.split(rfile, gfile, bfile)
        Parameters : 
            - rfile : basename of red layer
            - gfile : basename of green layer
            - bfile : basename of blue layer
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("split "  + rfile.replace(os.sep,'/')    # ne pas entourer de ' '
                                 +" " + gfile.replace(os.sep,'/') 
                                 +" " + bfile.replace(os.sep,'/') +" "))
    
    def split_cfa(self):
        """ Splits the CFA image into four distinct files (one for each channel) 
        and save them in files.
            
        Syntax     : status=app.split_cfa()
        Parameters : None
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("split_cfa"))

    def stack(self, seqname, type=None, rejection_type=None,
              filter_fwhm=None, filter_wfwhm=None, filter_round=None,filter_included=False,
              output_norm=False,filter_quality=None, norm=None, sigma_low=3, sigma_high=3, 
              weighted=False, out=None ):
        """ Stacks the seqfilename sequence, using options.

        The allowed types are: sum, max, min, med or median. The rejection type 
        is one of {p[ercentile] | s[igma] | m[edian] | w[insorized] | 
        l[inear] | g[eneralized]} for Percentile, Sigma, Median, Winsorized, 
        Linear-Fit or Generalized Extreme Studentized Deviate Test. If omitted, 
        the default (Winsorized) is used. The sigma low and high parameters of 
        rejection are mandatory.   
        If no argument other than the sequence name is provided, sum 
        stacking without filtering is assumed.

        See the tooltips in the stacking tab for more information about the 
        stacking methods and rejection types, or see the documentation.

        Best images from the sequence can be stacked by using the filtering 
        arguments.The filtering uses registration data, either with a value for 
        the last image to take depending on the type of data used (between 0 and 
        1 for roundness and quality) or a percentage of best images to keep if 
        the number is followed by a % sign. It is also possible to use manually 
        selected images, either previously from the GUI or with the select or 
        unselect commands.

        If several filters are added to the command, only images that pass all 
        the filters will be stacked. There is consequently no order. If a filter 
        is badly declared, because it has no registration data or a too low 
        threshold, nothing will be stacked.

        Normalization is automatically enabled for median and mean with 
        rejection stacking methods. This can be overridden using the -nonorm 
        flag or the -norm=normalization option. The allowed normalization are: 
        add, addscale, mul or mulscale. For other methods, or with the use 
        of the -nonorm flag, normalization is disabled.

        Output_norm applies a normalization at the end of the stacking to 
        rescale result in the [0, 1] range.
        
        Weighted is an option to add larger weights to frames with lower background noise.

        Stacked image for the sequence is created with the name provided in the 
        optional argument -out, or with the name of the sequence suffixed 
        "_stacked" and the configured FITS file extension. If a file with this 
        name already exists, it will be overwritten without warning.

        Note that this command was added in the 0.9.9 release, the filtering and 
        output naming options were added in the 0.9.11 release and the 
        output_norm in the 1.00.
            
        Syntax     : status=app.stack(seqfilename) 
                     status=app.stack(seqfilename, type={sum|min|max}, [filtering] [output_norm] [weighted] [out=filename])
                     status=app.stack(seqfilename, type={med|median},  [norm=no, norm=] [filter-included] [weighted] [out=filename])
                     status=app.stack(seqfilename, type={rej|mean}, [rejection_type], sigma_low, sigma_high, [nonorm,norm=] [filtering]  [weighted] [out=filename]

         Parameters :
            - sequencename    : basename of input images
            - type            : stack type (sum|min|max|med|median|rej|mean)
            - rejection_type  : The rejection type is one of : 
                                   {p[ercentile] , s[igma] , m[edian] , 
                                   w[insorized] , l[inear] , g[eneralized]}
            - filter_fwhm     : fwhm threshold ( absolu value or pourcent with % )
            - filter_wfwhm    : wfwhm threshold ( absolu value or pourcent with % )
            - filter_round    : roudness threshold ( absolu value or pourcent with % )
            - filter_included : 
            - output_norm     : normalization at the end of the stacking
            - weighted        : it is an option to add larger weights to frames with lower background noise.
            - filter_quality  : quality threshold ( absolu value or pourcent with % )
            - norm            : value are: no, add, addscale, mul or mulscale
            - sigma_low       : low threshold for sigma clipping rejection
            - sigma_high      : high threshold for sigma clipping rejection 
            - out             : output name of stacked image
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        if type==None :
            # stack seqfilename
            return (self.Execute("stack " + seqname ))
        
        parameters = " " 
        if ( type=="sum") or ( type=="min") or ( type=="max") :
            #  stack seqfilename { sum | min | max } [filtering] [-output_norm] [-out=filename]
            parameters += self.str_filtering(filter_fwhm, filter_wfwhm, filter_round,filter_included)
        else:
            if ( type=="med") or ( type=="median") :
                #  stack seqfilename { med | median } [-nonorm, norm=] [-filter-incl[uded]] [-out=filename]
                if norm != None :
                    if norm == "no" :
                        parameters+= "-nonorm "
                    else:
                        parameters+= "-norm=" + norm + " "
                if filter_included==True :
                    parameters += "-filter-included "
            else:
                #  stack seqfilename { rej | mean } sigma_low sigma_high [-nonorm, norm=] [filtering] [-out=filename]
                if rejection_type != None :
                    parameters += rejection_type[0].lower() + " "
                parameters += str( sigma_low ) + " " + str( sigma_high) + " "
                if norm != None :
                    if norm == "no" :
                        parameters+= "-nonorm "
                    else:
                        parameters+= "-norm=" + norm + " "
                parameters += self.str_filtering(filter_fwhm, filter_wfwhm, filter_round,filter_included)
                
        if output_norm :
            parameters+= "-output_norm "                
                
        if weighted :
            parameters+= "-weighted "    
                        
        if out != None :
            parameters+="-out=" + out.replace(os.sep,'/') +" "   # sans entourer de ' '         
        return (self.Execute("stack " + seqname + " " + type  + " "  + parameters ))

    def stackall(self, type=None,  rejection_type=None,
              filter_fwhm=None, filter_wfwhm=None, filter_round=None,filter_included=False,
              output_norm=False,filter_quality=None, norm=None, sigma_low=3, sigma_high=3, 
              weighted=False, out=None ):
        """ Opens all sequences in the current working directory (CWD) and 
        stacks them with the optionally specified stacking type or with sum 
        stacking. See stack commands for options description.

        Stacked images for each sequence are created with the suffix "_stacked" 
        and the configured FITS file extension.

        Note that most options for this command were introduced in the 0.9.8 
        release, the filtering options were introduced in the 0.9.11 release and 
        the output_norm option in the 1.0.0 release.  
          
        Syntax     : status=app.stackall(seqfilename) 
                     status=app.stackall(seqfilename, type={sum|min|max}, [filtering], [weighted], [out=filename])
                     status=app.stackall(seqfilename, type={med|median},  [norm=no, norm=], [filter-included], [-weighted], [out=filename])
                     status=app.stackall(seqfilename, type={rej|mean}, [rejection_type], sigma_low, sigma_high, [nonorm,norm=], [output_norm], [weighted], [filtering] [out=filename]
        Parameters :
            - sequencename    : basename of input images
            - type            : stack type (sum|min|max|med|median|rej|mean)
            - rejection_type  : The rejection type is one of : 
                                   {p[ercentile] , s[igma] , m[edian] , 
                                   w[insorized] , l[inear] , g[eneralized]}
            - filter_fwhm     : fwhm threshold ( absolu value or pourcent with % )
            - filter_wfwhm    : wfwhm threshold ( absolu value or pourcent with % )
            - filter_round    : roudness threshold ( absolu value or pourcent with % )
            - filter_included : 
            - output_norm     : normalization at the end of the stacking
            - weighted        : it is an option to add larger weights to frames with lower background noise.
            - filter_quality  : quality threshold ( absolu value or pourcent with % )
            - norm            : value are: no, add, addscale, mul or mulscale
            - sigma_low       : low threshold for sigma clipping rejection
            - sigma_high      : high threshold for sigma clipping rejection 
            - out             : output name of stacked image
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        if type==None :
            # stack seqfilename
            return (self.Execute("stackall"  ))
        
        parameters = " " 
        if ( type=="sum") or ( type=="min") or ( type=="max") :
            #  stack seqfilename { sum | min | max } [filtering] [-output_norm] [-out=filename]
            parameters += self.str_filtering(filter_fwhm, filter_wfwhm, filter_round,filter_included)
        else:
            if ( type=="med") or ( type=="median") :
                #  stack seqfilename { med | median } [-nonorm, norm=] [-filter-incl[uded]] [-out=filename]
                if norm != None :
                    if norm == "no" :
                        parameters+= "-nonorm "
                    else:
                        parameters+= "-norm=" + norm + " "
                if filter_included==True :
                    parameters += "-filter-included "
            else:
                #  stack seqfilename { rej | mean } sigma_low sigma_high [-nonorm, norm=] [filtering] [-out=filename]
                if rejection_type != None :
                    parameters += rejection_type[0].lower() + " "
                parameters += str( sigma_low ) + " " + str( sigma_high) + " "
                if norm != none :
                    if norm == "no" :
                        parameters+= "-nonorm "
                    else:
                        parameters+= "-norm=" + norm + " "
                parameters += self.str_filtering(filter_fwhm, filter_wfwhm, filter_round,filter_included)                
        if output_norm :
            parameters+= "-output_norm "
        if weighted :
            parameters+= "-weighted "    
        if out != None :
            parameters+="-out=" + out.replace(os.sep,'/') +" "  # sans entourer de ' '           
        return (self.Execute("stackall " + type  + " "  + parameters ))

    def stat(self):
        """ Returns global statistic of the current image. If a selection is 
        made, the command returns global statistic within the selection.
            
        Syntax     : status=app.stat()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
            - status[1] = statistic values of the current image
        """
        status = self.Execute("stat", bEndTest = True  )
        if status is False :
            return (False)
        
        log = self.filt_log(self.GetData())[1:]
        
        res_stat =[] 
        for line in log : 
            rr = line.replace( ' ', '').replace(',',':').split(':')[1:]
            label=rr[0].lower().replace('layer','').replace('canal','')
            res_stat.append({ "layer"  : label.upper(),
                              "mean"   : float(rr[2]), 
                              "median" : float(rr[4]),             
                              "sigma"  : float(rr[6]),             
                              "avgdev" : float(rr[8]),             
                              "min"    : float(rr[10]),             
                              "max"    : float(rr[12])             
                            })
        return (status, res_stat )
    
    def subsky(self, degree):
        """ Computes the level of the local sky background thanks to a 
        polynomial function of an order degree and subtracts it from the image. 
        A synthetic image is then created and subtracted from the original one.
            
        Syntax     : status=app.subsky(degree)
        Parameters : 
            - degree: order degree of the polynomial function
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("subsky " + str(degree) ))
    
    def threshlo(self,value):
        """ replaces values below threshold with threshold
            
        Syntax     : status=app.threshlo(value)
        Parameters : 
            - value: low threshold 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("threshlo " + str(value) ))

    def threshhi(self,value):
        """ replaces values above threshold with threshold
            
        Syntax     : status=app.threshhi(value)
        Parameters : 
            - value: high threshold 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("threshhi " + str(value) ))    
    
    def thresh(self,low,high):
        """ replaces values below low threshold with low threshold and
        replaces values above high threshold with high threshold
            
        Syntax     : status=app.thresh(low,high)
        Parameters : 
            - low  : low threshold 
            - value: high threshold 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("thresh " + str(low) + " " +str(high) )) 

    ''' Non scriptable 
    def unselect(self,first,last):
        """ Allows easy mass unselection of images in the loaded sequence 
        (first-last). 
        See SELECT.
            
        Syntax     : status=app.unselect(first,last)
        Parameters : 
            - first index
            - last index 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("unselect " + first + " " + last))   
    '''
        
    ''' Non scriptable 
    def unsetmag(self):
        """ Reset the magnitude calibration to 0. 
        See SETMAG.
            
        Syntax     : status=app.unsetmag()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("unsetmag"  ))
    '''
    
    ''' Non scriptable 
    def unsetmagseq(self):
        """ Resets the magnitude calibration and reference star for the sequence. 
        See SETMAGSEQ.
            
        Syntax     : status=app.unsetmagseq()
        Parameters : None 
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("unsetmagseq"  ))
    '''
    
    def unsharp(self, sigma, multi):
        """ Applies to the working image an unsharp mask with sigma sigma and 
        coefficient multi.
            
        Syntax     : status=app.unsharp(sigma, multi)
        Parameters : 
            - sigma
            - multi
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("unsharp " + str(sigma) + " " + str(multi) ))
    
    def wavelet(self, plan_number, type):
        """ Computes the wavelet transform on "nbr_plan" plans using linear 
        (type=1) or bspline (type=2) version of the 'a trous' algorithm. The 
        result is stored in a file as a structure containing the planes, ready 
        for weighted reconstruction with WRECONS.
            
        Syntax     : status=app.wavelet(plan_number, type)
        Parameters : 
            - plan_number : number of plans
            - type        : 1=linear , 2=bspline
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        return (self.Execute("unsharp " + str(plan_number) + " " + str(type) ))
    
    def wrecons(self, list_c):
        """ Reconstructs to current image from the planes previously computed 
        with wavelets and weighted with coefficients c1, c2, ..., cn according 
        to the number of planes used for wavelet transform. 
            
        Syntax     : status=app.wrecons(list_coef)
        Parameters : 
            - list_coef : coefficients list according to the number of planes
        Return     : 
            - status[0] = True: execution ok /status[0] = False: Aborted
        """
        coefficients=""
        for c in list_c :
            coefficients +=  str(c) + " "
        return (self.Execute("wrecons " + coefficients ))

