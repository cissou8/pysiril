# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: pysiril ( Python SiriL )
#
# This is a library to interface python to Siril.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

tabulation= '         '
helpmsg= {
##     ''' Non scriptable 
##     "addmax" : {
##         "syntax1" : tabulation + 'app.Execute("addmax filename")',
##         "syntax2" : tabulation + 'status=app.addmax(filename)',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Computes a new image combining the image in memory with the image filename. 
## At each pixel location, the new value is determined as the max of value in 
## current image and in filename.
## Do not forget to save the result.
## """) },
##     '''
    
    "asinh" : {
        "syntax1" : tabulation + 'app.Execute("asinh stretch")',
        "syntax2" : tabulation + 'status=app.asinh(stretch)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _(""" 
Stretches the image to show faint objects, while simultaneously, 
preserves the structure of bright objects of the field.
""") },
    
    "bg" : {
        "syntax1" : tabulation + 'app.Execute("bg")',
        "syntax2" : tabulation + 'status=app.bg()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted') + 
                    '\n' + tabulation + _("status[1] = background level"),
        "description" : _("""\
Returns the background level of the image loaded in memory.
""") },
    
    "bgnoise" : {
        "syntax1" : tabulation + 'app.Execute("bgnoise")',
        "syntax2" : tabulation + 'status=app.bgnoise()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted') + 
                    '\n' + tabulation + _("status[1] = background level"),
        "description" : _("""\
Returns the background noise level of the image loaded in memory.
""") },
    
##     ''' Non scriptable 
##     "boxselect" : {
##         "syntax1" : tabulation + 'app.Execute("boxselect x y width height")',
##         "syntax2" : tabulation + 'status=app.boxselect(x, y, width, height)',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Make a selection with the arguments x, y, width and height, 
## with x and y being the coordinates of the top left corner, 
## and width and height, the size of the selection. 
## If no argument is passed and a selection is active, 
## it writes x, y, width and height in the console
## """) },
##     '''
    
    "cd" : {
        "syntax1" : tabulation + 'app.Execute("cd directory")',
        "syntax2" : tabulation + 'status=app.cd(directory)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Sets the new current working directory.
The argument directory can contain the ~ token, expanded as the home directory, 
directories with spaces in the name can be protected using single or double quotes
Examples:
    cd ~/M42
    cd '../OIII 2x2/'
""") },
    
    "cdg" : {
        "syntax1" : tabulation + 'app.Execute("cdg")',
        "syntax2" : tabulation + 'status=app.cdg()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted')+ 
                    '\n' + tabulation + _("status[1] = x ") +
                    '\n'+ _("status[2] = y "),
        "description" : _("""\
Returns the coordinates of the center of gravity of the image
""") },
    
    "clahe" : {
        "syntax1" : tabulation + 'app.Execute("clahe cliplimit tileSize")',
        "syntax2" : tabulation + 'status=app.clahe(cliplimit, tileSize)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Equalizes the histogram of an image using Contrast Limited Adaptive Histogram Equalization.
   o cliplimit sets the threshold for contrast limiting.
   o tilesize sets the size of grid for histogram equalization. 
Input image will be divided into equally sized rectangular tiles.
""") },
    
##     ''' Non scriptable 
##     "clear" : {
##         "syntax1" : tabulation + 'app.Execute("clear")',
##         "syntax2" : tabulation + 'status=app.clear()',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Clears the graphical output logs
## """) },
##     '''
##     
##     ''' Non scriptable 
##     "clearstar" : {
##         "syntax1" : tabulation + 'app.Execute("clearstar")',
##         "syntax2" : tabulation + 'status=app.clearstar()',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Clear all the stars saved in memory and displayed on the screen.
## """) },
##     '''

    "close" : {
        "syntax1" : tabulation + 'app.Execute("close")',
        "syntax2" : tabulation + 'status=app.close()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Properly closes the opened image and the opened sequence, if any.
""") },

    "convert" : {
        "syntax1" : tabulation + 'app.Execute("basename [-debayer] [-start=index] [-out=] [-fitseq] [-ser]")',
        "syntax2" : tabulation + 'status=app.convert(basename,[debayer=True] [start=index] [out=filename] [fitseq=True] [ser=True])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Converts all images in a known format into Siril's FITS images.
    o The argument basename is the basename of the new sequence. 
      For FITS images, Siril will try to make a symbolic link. 
      If not possible, files will be copied.
    o The flags -fitseq and -ser can be used to specify an alternative output 
      format, other than the default FITS.
    o The option -debayer applies demosaicing to images. 
      In this case no symbolic link are done.
    o -start=index sets the starting index number 
    o -out= option converts files into the directory out.
""") },

    "convertraw" : {
        "syntax1" : tabulation + 'app.Execute("convertraw basename [-debayer] [-start=index] [-out=] [-fitseq] [-ser]")',
        "syntax2" : tabulation + 'status=app.convertraw(basename,[debayer=True] [start=index] [out=filename] [fitseq=True] [ser=True] )',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Converts DSLR RAW files into Siril's FITS images.
    o The argument basename is the basename of the new sequence. 
      For FITS images, Siril will try to make a symbolic link. 
      If not possible, files will be copied.
    o The flags -fitseq and -ser can be used to specify an alternative output 
      format, other than the default FITS.
    o The option -debayer applies demosaicing to images. 
      In this case no symbolic link are done.
    o -start=index sets the starting index number 
    o -out= option converts files into the directory out
""") },

    "cosme" : {
        "syntax1" : tabulation + 'app.Execute("cosme filename")',
        "syntax2" : tabulation + 'status=app.cosme(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Applies the local mean to a set of pixels on the in-memory image (cosmetic correction). 
The coordinates of these pixels are in an ASCII file [.lst file]. COSME is adapted 
to correct residual hot and cold pixels after preprocessing")

The line P x y type will fix the pixel at coordinates (x, y) type is an optional 
character (C or H) specifying to Siril if the current pixel is cold or hot. This 
line is created by the command find_hot but you also can add some lines manually.
The line C x 0 type will fix the bad column at coordinates x.
The line L y 0 type will fix the bad line at coordinates y.
""") },

    "cosme_cfa" : {
        "syntax1" : tabulation + 'app.Execute("cosme_cfa filename")',
        "syntax2" : tabulation + 'status=app.cosme_cfa(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same function that COSME but applying to RAW CFA images. 
""") },

    "crop" : {
        "syntax1" : tabulation + 'app.Execute("crop x y width height")',
        "syntax2" : tabulation + 'status=app.crop(x, y, width, height)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Crops a selection of the loaded image.
In the GUI, if a selection is active, no further arguments are required. 
Otherwise, or in scripts, arguments have to be given, with x and y being 
the coordinates of the top left corner, and width and height the size of 
the selection.
""") },

##     ''' Non scriptable 
##     "ddp" : {
##         "syntax1" : tabulation + 'app.Execute("ddp level coef sigma")',
##         "syntax2" : tabulation + 'status=app.ddp(level, coef, sigma)',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Performs a DDP (digital development processing) as described first by Kunihiko 
## Okano. This implementation is the one described in IRIS.
## It combines a linear distribution on low levels (below level) and a non-linear 
## one on high levels. It uses a Gaussian filter of standard deviation sigma and 
## multiplies the resulting image by coef. Typical values for sigma are within 
## 0.7 and 2".
## """) },
##     '''

    "entropy" : {
        "syntax1" : tabulation + 'app.Execute("entropy")',
        "syntax2" : tabulation + 'status=app.entropy()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted')+ 
                    '\n' + tabulation + _("status[1] = entropy value"),
        "description" : _("""\
Computes the entropy of the opened image on the displayed layer, 
only in the selected area if one has been selected or in the whole image. 
The entropy is one way of measuring the noise or the details in an image.
""") },

    "exit" : {
        "syntax1" : tabulation + 'app.Execute("exit")',
        "syntax2" : tabulation + 'status=app.exit()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Quits the application.
""") },

    "extract" : {
        "syntax1" : tabulation + 'app.Execute("extract NbPlans")',
        "syntax2" : tabulation + 'status=app.extract(NbPlans)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Extracts NbPlans Planes of Wavelet domain.
""") },

    "extract_Ha" : {
        "syntax1" : tabulation + 'app.Execute("extract_Ha")',
        "syntax2" : tabulation + 'status=app.extract_Ha()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Extracts Ha signal from a CFA image. The output file name starts with the 
prefix "Ha_".
""") },

    "extract_HaOIII" : {
        "syntax1" : tabulation + 'app.Execute("extract_HaOIII")',
        "syntax2" : tabulation + 'status=app.extract_HaOIII()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Extracts Ha and OIII signals from a CFA image. The output file name start with 
the prefix "Ha_" and "OIII_".
""") },

    "extract_Green" : {
        "syntax1" : tabulation + 'app.Execute("extract_Green")',
        "syntax2" : tabulation + 'status=app.extract_Green()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Extracts green signal from a CFA image. The output file name starts with the 
prefix "Green_".
""") },

    "fdiv" : {
        "syntax1" : tabulation + 'app.Execute("fdiv filename scalar")',
        "syntax2" : tabulation + 'status=app.fdiv(filename, scalar)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Divides the image in memory by the image given in argument. The resulting image 
is multiplied by the value of the scalar argument. See also idiv.
""") },

    "fftd" : {
        "syntax1" : tabulation + 'app.Execute("fftd modulus phase")',
        "syntax2" : tabulation + 'status=app.fftd(modulus, phase)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Applies a Fast Fourier Transform to the image loaded in memory. Modulus and phase 
given in argument are saved in FITS files.
""") },

    "ffti" : {
        "syntax1" : tabulation + 'app.Execute("ffti modulus phase")',
        "syntax2" : tabulation + 'status=app.ffti(modulus, phase)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Retrieves corrected image applying an inverse transformation. 
The modulus and phase used are the files given in argument.
""") },

    "fill" : {
        "syntax1" : tabulation + 'app.Execute("fill value x y width height")',
        "syntax2" : tabulation + 'status=app.fill(value, x, y, width, height)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Fills the whole current image (or selection) with pixels having the value 
intensity expressed in ADU..
""") },

    "fill2" : {
        "syntax1" : tabulation + 'app.Execute("fill2 value x y width height")',
        "syntax2" : tabulation + 'status=app.fill2(value, x, y, width, height)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command as FILL but this is a symmetric fill of a region defined by the mouse. 
Used to process an image in the Fourier (FFT) domain.
""") },

    "find_cosme" : {
        "syntax1" : tabulation + 'app.Execute("find_cosme cold_sigma hot_sigma")',
        "syntax2" : tabulation + 'status=app.find_cosme(cold_sigma, hot_sigma)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Applies an automatic detection of cold and hot pixels following the thresholds 
written in arguments.
""") },

    "find_cosme_cfa" : {
        "syntax1" : tabulation + 'app.Execute("find_cosme_cfa cold_sigma hot_sigma")',
        "syntax2" : tabulation + 'status=app.find_cosme_cfa(cold_sigma, hot_sigma)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command than FIND_COSME but for monochromatic CFA images.
""") },

    "find_hot" : {
        "syntax1" : tabulation + 'app.Execute("find_hot filename cold_sigma hot_sigma")',
        "syntax2" : tabulation + 'status=app.find_hot(filename,cold_sigma, hot_sigma)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Saves a list file filename (text format) in the working directory which contains 
the coordinates of the pixels which have an intensity hot_sigma times higher and 
cold_sigma lower than standard deviation. We generally use this command on a 
master-dark file.
""") },

##     ''' Non scriptable 
##     "findstar" : {
##         "syntax1" : tabulation + 'app.Execute("findstar")',
##         "syntax2" : tabulation + 'status=app.findstar()',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Detects stars having a level greater than a threshold computed by Siril.
## The algorithm is based on the publication of Mighell, K. J. 1999, 
## in ASP Conf. Ser., Vol. 172, Astronomical Data Analysis Software and 
## Systems VIII, eds. D. M. Mehringer, R. L. Plante, and D. A. Roberts 
## (San Francisco: ASP), 317.
## After that, a PSF is applied and Siril rejects all detected structures that 
## don't fulfill a set of prescribed detection criteria. Finally, a circle is 
## drawn around detected stars.
## See also the command CLEARSTAR.
## """) },
##     '''

    "fixbanding" : {
        "syntax1" : tabulation + 'app.Execute("fixbanding amount sigma")',
        "syntax2" : tabulation + 'status=app.fixbanding(amount,sigma)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Tries to remove the canon banding.
    o Argument amount define the amount of correction. 
    o Sigma defines a protection level of the algorithm, higher sigma gives 
      higher protection.
""") },

    "fix_xtrans" : {
        "syntax1" : tabulation + 'app.Execute("fix_xtrans")',
        "syntax2" : tabulation + 'status=app.fix_xtrans()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Fixes the Fujifilm X-Trans Auto Focus pixels.
Indeed, because of the phase detection auto focus system, the photosites used 
for auto focus get a little less light than the surrounding photosites. The 
camera compensates for this and increases the values from these specific 
photosites giving a visible square in the middle of the dark/bias frames.
""") },


    "fmedian" : {
        "syntax1" : tabulation + 'app.Execute("fmedian ksize modulation")',
        "syntax2" : tabulation + 'status=app.fmedian(ksize, modulation)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Performs a median filter of size ksize x ksize (ksize MUST be odd) to the  
original image with a modulation parameter modulation.
The output pixel is computed as : out=mod x m+(1−mod) x in, where m is the 
median-filtered pixel value. A modulation's value of 1 will apply no modulation.
""") },

    "fmul" : {
        "syntax1" : tabulation + 'app.Execute("fmul scalar")',
        "syntax2" : tabulation + 'status=app.fmul(scalar)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Multiplies the loaded image by the scalar given in argument.
""") },

    "gauss" : {
        "syntax1" : tabulation + 'app.Execute("gauss sigma")',
        "syntax2" : tabulation + 'status=app.gauss(sigma)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Performs a Gaussian filter with the given sigma.
""") },

    "grey_flat" : {
        "syntax1" : tabulation + 'app.Execute("grey_flat")',
        "syntax2" : tabulation + 'status=app.grey_flat()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Equalizes the mean intensity of RGB layers in a CFA image.
""") },

    "histo" : {
        "syntax1" : tabulation + 'app.Execute("histo layer")',
        "syntax2" : tabulation + 'status=app.histo()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Calculates the histogram of the image channel in memory and produces file 
histo_[channel name].dat in the working directory".

layer = 0, 1 or 2 with 0=red, 1=green and 2=blue.
""") },

    "iadd" : {
        "syntax1" : tabulation + 'app.Execute("iadd filename")',
        "syntax2" : tabulation + 'status=app.iadd(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Adds the image in memory to the image filename given in argument.
""") },

    "idiv" : {
        "syntax1" : tabulation + 'app.Execute("idiv filename")',
        "syntax2" : tabulation + 'status=app.idiv(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Divides the image in memory by the image filename given in argument. 
See also FDIV.
""") },

    "imul" : {
        "syntax1" : tabulation + 'app.Execute("imul filename")',
        "syntax2" : tabulation + 'status=app.imul(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Multiplies the image in memory by the image filename given in argument.
""") },

    "isub" : {
        "syntax1" : tabulation + 'app.Execute("isub filename")',
        "syntax2" : tabulation + 'status=app.isub(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Subtracts the image in memory by the image filename given in argument.
""") },

    "linear_match" : {
        "syntax1" : tabulation + 'app.Execute("linear_match reference low high")',
        "syntax2" : tabulation + 'status=app.linear_match(reference, low, high)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Computes a linear function between a reference image and the image in memory.
The function is then applied to the current image to match it to the reference one. 
The algorithm will ignore all reference pixels whose values are outside of the 
[low, high] range".
""") },

    "link" : {
        "syntax1" : tabulation + 'app.Execute("link basename [-start=index] [-out=directory]")',
        "syntax2" : tabulation + 'status=app.link(basename, [start=index] [out=directory])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Links all FITS images in the working directory with the basename given in argument.
If no symbolic links could be created, files are copied. It is possible to convert 
files in another directory with the -out= option.
""") },

    "load" : {
        "syntax1" : tabulation + 'app.Execute("load filename")',
        "syntax2" : tabulation + 'status=app.load(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Loads the image filename
It first attempts to load filename, then filename.fit, finally filename.fits 
and finally all supported formats, aborting if none of these are found.
This scheme is applicable to every Siril command that involves reading files.
Fits headers MIPS-HI and MIPS-LO are read and their values given to the current 
viewing levels.
Writing a known extension .ext at the end of filename will load specifically the 
image filename.ext: this is used when numerous files have the same name but not 
the same extension.

Extensions supported are :
    *.fit, *.fits, *.fts
    *.bmp / *.jpg, *.jpeg / *.png / *.tif, *.tiff
    *.ppm, *.pgm
    *.pic (IRIS file)
""") },

    "log" : {
        "syntax1" : tabulation + 'app.Execute("")',
        "syntax2" : tabulation + 'status=app.log()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Computes and applies a logarithmic scale to the current image.
""") },

    "merge" : {
        "syntax1" : tabulation + 'app.Execute("merge sequence1 sequence2 [sequence3 ...] output_sequence")',
        "syntax2" : tabulation + 'status=app.merge(list_seq, new_seq )',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Merges several sequences of the same type (FITS images, FITS sequence or SER) 
and same image properties into a new sequence with base name newseq created in 
the current working directory, with the same type. 
The input sequences can be in different directories, can specified either 
in absolute or relative path, with the exact .seq name or with only 
the base name with or without the trailing '_'. 
""") },


    "mirrorx" : {
        "syntax1" : tabulation + 'app.Execute("mirrorx")',
        "syntax2" : tabulation + 'status=app.mirrorx()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Rotates the image around a vertical axis.
""") },

    "mirrory" : {
        "syntax1" : tabulation + 'app.Execute("mirrory")',
        "syntax2" : tabulation + 'status=app.mirrory()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Rotates the image around an horizontal axis.
""") },

    "mtf" : {
        "syntax1" : tabulation + 'app.Execute("mtf low midtone high")',
        "syntax2" : tabulation + 'status=app.mtf(low, midtone, high)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Applies midtones transfer function to the current loaded image.
Three parameters are needed, low, midtones and high where midtones balance 
parameter defines a nonlinear histogram stretch in the [0,1] range.
""") },

    "neg" : {
        "syntax1" : tabulation + 'app.Execute("neg")',
        "syntax2" : tabulation + 'status=app.neg()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Shows the negative view of the current image.
""") },

##     ''' Non scriptable 
##     "new" : {
##         "syntax1" : tabulation + 'app.Execute("new width height nb_layers")',
##         "syntax2" : tabulation + 'status=app.new(width, height , nb_layers)',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Creates a new image filled with zeros with a size of width x height.
## The image is in 32-bit format, and it contains nb_channel channels, nb_channel 
## being 1 or 3. It is not saved, but displayed and can be saved afterwards.
## """) }, 
##     '''  

    "nozero" : {
        "syntax1" : tabulation + 'app.Execute("nozero level")',
        "syntax2" : tabulation + 'status=app.nozero(level)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Replaces null values by level values. Useful before an idiv or fdiv operation.
""") },

    "offset" : {
        "syntax1" : tabulation + 'app.Execute("offset value")',
        "syntax2" : tabulation + 'status=app.offset(value)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Adds the constant value (specified in ADU) to the current image. This constant 
can take a negative value.
In 16-bit mode, values of pixels that fall outside of [0, 65535] are clipped. 
In 32-bit mode, no clipping occurs.
""") },

    "preprocess" : {
        "syntax1" : tabulation + 'app.Execute("preprocess sequencename [-bias=filename] [-dark=filename] [-flat=filename] [-cfa] [-debayer] [-fix_xtrans] [-flip] [-equalize_cfa] [-opt] [-prefix=] [-fitseq]")',
        "syntax2" : tabulation + 'status=app.preprocess(sequencename, [bias=filename], [dark=filename], [flat=filename], [cfa], [debayer=True], [fix_xtrans=true], [flip=True], [equalize_cfa=True], [opt=True], [prefix=xxx], [fitseq=True])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Preprocesses the sequence sequencename using bias, dark and flat given in argument

o For bias, a uniform level can be specified instead of an image, by entering a quoted 
expression starting with an = sign, such as -bias="=256" or  -bias="=64*$OFFSET".

o It is possible to specify if images are CFA for cosmetic correction purposes 
with the option -cfa and also to demosaic images at the end of the process with 
-debayer.

o The -fix_xtrans option is dedicated to X-Trans files by applying a correction 
on darks and biases to remove an ugly square pattern.

o The -equalize_cfa option equalizes the mean intensity of RGB layers of the CFA 
flat master.

o It is also possible to optimize the dark subtraction with -opt.

o The output sequence name starts with the prefix "pp_" unless otherwise specified
with option -prefix=.

o If -fitseq is provided, the output sequence will be a FITS sequence (single file).

Note that only hot pixels are corrected in cosmetic correction process.

""") },

##     ''' Non scriptable 
##     "psf" : {
##         "syntax1" : tabulation + 'app.Execute("psf")',
##         "syntax2" : tabulation + '# NOT IMPLEMENTED #',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Performs a PSF (Point Spread Function) on the selected star.
## Make a selection around a star and call the command PSF. It will give you:
## 
##     The centroid coordinates (x0 and y0) in pixel units, which is the position of the center of symmetry of the fitted PSF.
##     The FWHM on the X and Y axis.
##     The rotation angle of the X axis with respect to the centroid coordinates.
##     The average local background.
##     The maximal intensity of the star: this is the peak value of the fitted function, located at the centroid coordinates x0 and y0.
##     The relative magnitude of the star.
##     The RMSE. This is an estimate of fitting quality. The smaller the RMSE is, the better the function is fitted.
## 
## To be relevant, the selection MUST be done on a non-saturated star.
## """) },
##     '''
    
    "register" : {
        "syntax1" : tabulation + 'app.Execute("register sequence [-norot] [-drizzle] [-prefix=] [-minpairs=] [-transf=] [-layer=]")',
        "syntax2" : tabulation + 'status=app.register(sequence, [norot=True] [drizzle=True] [prefix=xxx])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Performs geometric transforms on images of the sequence given in argument so that 
they may be superimposed on the reference image. Using stars for registration, 
this algorithm only works with deepsky images.

o The output sequence name starts  with the prefix "r_" unless otherwise specified 
with -prefix= option.

o The option -drizzle activates the sub-pixel stacking, either by up-scaling by 2 
the images created in the rotated sequence or by setting a flag that will proceed 
to the up-scaling during stacking if -norot is passed.

o The option -transf= specifies the use of either "shift", "similarity", "affine" 
  or "homography" transformations respectively, homography being the default 
  unless -norot is passed, which uses shift as default.
  
o The option -minpairs= will specify the minimum number of star pairs a frame must 
  have with the reference frame, otherwise the frame will be dropped.

o The registration is done on the green layer for RGB images unless specified by 
  -layer= option (0, 1 or 2 for R, G and B respectively).

""") },

    "requires" : {
        "syntax1" : tabulation + 'app.Execute("requires x.y.z")',
        "syntax2" : tabulation + 'status=app.resample("x.y.z")',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Returns an error if the version of Siril is older than the one passed in argument.
ie: requires 0.99.6
""") },

    "resample" : {
        "syntax1" : tabulation + 'app.Execute("resample factor")',
        "syntax2" : tabulation + 'status=app.resample(factor)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Resamples image with a factor "factor".
""") },

    "rgradient" : {
        "syntax1" : tabulation + 'app.Execute("rgradient xc yc dR dalpha")',
        "syntax2" : tabulation + 'status=app.rgradient(xc, yc, dR, dalpha)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Creates two images, with a radial shift (dR in pixels) and a rotational shift 
(dalpha in degrees) with respect to the point (xc, yc).

Between these two images, the shifts have the same amplitude, but an opposite sign. 
The two images are then added to create the final image. This process is also 
called Larson Sekanina filter
""") },

    "rl" : {
        "syntax1" : tabulation + 'app.Execute("rl threshold sigma corner_radius_boost iterations")',
        "syntax2" : tabulation + 'status=app.rl(threshold, sigma, corner_radius_boost, iterations)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Restores an image using the Richardson-Lucy method.

o Sigma is the size of the kernel to be applied, while corner_radius_boost is a 
 value which is added to Gaussian sigma for the tiles in the corners of an image.
o Iterations is the number of iterations to be performed

""") },

    "rmgreen" : {
        "syntax1" : tabulation + 'app.Execute("rmgreen type")',
        "syntax2" : tabulation + 'status=app.rmgreen(type)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Applies a chromatic noise reduction filter. It removes green noise in the current 
image. This filter is based on PixInsight's SCNR Average Neutral algorithm and 
it is the same filter used by HLVG plugin in Photoshop.

Type=1 stands for Average Neutral Protection, while type=2 stands for Maximum 
Neutral Protection.
""") },

    "rotate" : {
        "syntax1" : tabulation + 'app.Execute("rotate degree, [-nocrop]")',
        "syntax2" : tabulation + 'status=app.rotate(degree,[nocrop=True])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Rotates the image by an angle of degree value. 
The option -nocrop can be added to avoid the cropping
""") },

    "rotatepi" : {
        "syntax1" : tabulation + 'app.Execute("rotatepi")',
        "syntax2" : tabulation + 'status=app.rotatepi()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Rotates the image of an angle of 180° around its center. This is equivalent to 
the command "ROTATE 180" or "ROTATE -180"
""") },

    "satu" : {
        "syntax1" : tabulation + 'app.Execute("satu coeff")',
        "syntax2" : tabulation + 'status=app.satu(coeff)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Enhances the global saturation of the image. Try iteratively to obtain best results.
""") },

    "save" : {
        "syntax1" : tabulation + 'app.Execute("save filename")',
        "syntax2" : tabulation + 'status=app.save(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Saves current image to filename.fit (or .fits, depending on your preferences, 
see SETEXT). Fits headers MIPS-HI and MIPS-LO are added with values corresponding 
to the current viewing levels.
""") },

    "savebmp" : {
        "syntax1" : tabulation + 'app.Execute("savebmp filename")',
        "syntax2" : tabulation + 'status=app.savebmp(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Saves current image under the form of a bitmap file with 8bits per channel: 
filename.bmp (BMP 24 bits).
""") },

    "savejpg" : {
        "syntax1" : tabulation + 'app.Execute("savejpg filename [quality]")',
        "syntax2" : tabulation + 'status=app.savejpg(filename, [quality=100])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Saves current image into a JPG file: filename.jpg.

You have the possibility to adjust the quality of the compression. A value 100 
for quality parameter offers best fidelity while a low value increases the 
compression ratio. If no value is specified, a default value of 100 is applied.
""") },

    "savepng" : {
        "syntax1" : tabulation + 'app.Execute("savepng filename")',
        "syntax2" : tabulation + 'status=app.savepng(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Saves current image as a PNG file.
""") },

    "savepnm" : {
        "syntax1" : tabulation + 'app.Execute("savepnm filename")',
        "syntax2" : tabulation + 'status=app.savepnm(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Saves current image under the form of a Netpbm file format with 16-bit per channel.

The extension of the output will be filename.ppm for RGB image and filename.pgm 
for gray-level image

More details about the Netpbm format at : http://en.wikipedia.org/wiki/Netpbm_format.
""") },

    "savetif" : {
        "syntax1" : tabulation + 'app.Execute("savetif filename")',
        "syntax2" : tabulation + 'status=app.savetif(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Saves current image under the form of a uncompressed TIFF file with 16bits per channel.
""") },

    "savetif32" : {
        "syntax1" : tabulation + 'app.Execute("savetif32 filename")',
        "syntax2" : tabulation + 'status=app.savetif32(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command than SAVE_TIF but the output file is saved in 32bits per channel.
""") },

    "savetif8" : {
        "syntax1" : tabulation + 'app.Execute("savetif8 filename")',
        "syntax2" : tabulation + 'status=app.savetif8(filename)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command as SAVETIF but the output file is saved in 32-bit per channel: filename.tif.
""") },

##     ''' Non scriptable 
##     "select" : {
##         "syntax1" : tabulation + 'app.Execute("select from to")',
##         "syntax2" : tabulation + 'status=app.select(from, to)',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## This command allows easy mass selection of images in the loaded sequence
## (from - to, to included). 
## Examples:
##     - selects the first.
##         select 0 0
##     - selects 201 images starting from number 1000
##         select 1000 1200
## The second number can be greater than the number of images to just go up to the end.
## 
## See UNSELECT.
## """) },
##     '''
    
    "seqextract_Green" : {
        "syntax1" : tabulation + 'app.Execute("seqextract_Green seqname [-prefix=]")',
        "syntax2" : tabulation + 'status=app.seqextract_Green(seqname,[prefix=xxx])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command than extract_Green but for the sequence "seqname". 
The output sequence name starts with the prefix "Green_" unless otherwise 
specified with option "-prefix=".
""") },

    "seqextract_Ha" : {
        "syntax1" : tabulation + 'app.Execute("seqextract_Ha seqname [-prefix=]")',
        "syntax2" : tabulation + 'status=app.seqextract_Ha(seqname,[prefix=xxx])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command than extract_Ha but for the sequence "seqname". 
The output sequence name starts with the prefix "Ha_" unless otherwise 
specified with option "-prefix=".
""") },

    "seqextract_HaOIII" : {
        "syntax1" : tabulation + 'app.Execute("seqextract_HaOIII seqname [-prefix=]")',
        "syntax2" : tabulation + 'status=app.seqextract_HaOIII(seqname,[prefix=xxx])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command than EXTRACT_HAOIII but for the sequence "seqname". The output 
sequence name start with the prefix "Ha_" and "OIII_".
""") },
    
    "seqcosme" : {
        "syntax1" : tabulation + 'app.Execute("seqcosme sequencename [filename].lst [-prefix=]")',
        "syntax2" : tabulation + 'status=app.seqcosme(seqname,[filename].lst, [prefix=xxx])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command as COSME but for the the sequence sequencename.

The output sequence name starts with the prefix "cosme_" unless otherwise  
specified with option "-prefix="
""") },
    
    "seqcosme_cfa" : {
        "syntax1" : tabulation + 'app.Execute("seqcosme_cfa sequencename [filename].lst [-prefix=]")',
        "syntax2" : tabulation + 'status=app.seqcosme_cfa(seqname,[filename].lst, [prefix=xxx])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command as COSME but for the the sequence sequencename.

The output sequence name starts with the prefix "cosme_" unless otherwise  
specified with option "-prefix="
""") },

    "seqcrop" : {
        "syntax1" : tabulation + 'app.Execute("seqcrop [-prefix=xxx]")',
        "syntax2" : tabulation + 'status=app.seqcrop([prefix=xxx])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command as COSME but for the the sequence sequencename.
The output sequence name starts with the prefix "cosme_" unless otherwise 
specified with option "-prefix="
""") },

    "seqfind_cosme" : {
        "syntax1" : tabulation + 'app.Execute("seqfind_cosme cold_sigma hot_sigma [-prefix=xxx]")',
        "syntax2" : tabulation + 'status=app.seqfind_cosme(cold_sigma, hot_sigma, [prefix=xxx])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command as FIND_COSME but for the sequence sequencename.
The output sequence name starts with the prefix "cc_" unless otherwise specified 
with -prefix= option .
""") },

    "seqfind_cosme_cfa" : {
        "syntax1" : tabulation + 'app.Execute("seqfind_cosme_cfa cold_sigma hot_sigma [-prefix=xxx]")',
        "syntax2" : tabulation + 'status=app.seqfind_cosme_cfa(cold_sigma, hot_sigma, -prefix=xxx])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command as FIND_COSME_CFA but for the sequence sequencename.
The output sequence name starts with the prefix "cc_" unless otherwise specified 
with -prefix= option .
""") },

    "seqmtf" : {
        "syntax1" : tabulation + 'app.Execute("seqmtf seqname low midtone high [prefix=xxx]")',
        "syntax2" : tabulation + 'status=app.seqmtf()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command than MTF but for the sequence seqname. The output sequence name 
starts with the prefix "mtf_" unless otherwise specified with "-prefix=" option.
""") },

##     ''' Non scriptable 
##     "seqpsf" : {
##         "syntax1" : tabulation + 'app.Execute("seqpsf")',
##         "syntax2" : tabulation + '# NOT IMPLEMENTED #',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Same command as PSF but works for sequences. Results are dumped in the console 
## in a form that can be used to produce brightness variation curves
## """) },
##     '''
    "seqsplit_cfa" : {
        "syntax1" : tabulation + 'app.Execute("seqsplit_cfa seqname [-prefix=xxx]")',
        "syntax2" : tabulation + 'status=app.seqsplit_cfa(seqname, [prefix=xxx])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command as SPLIT_CFA but for the sequence sequencename. The output sequences 
names start with the prefix "CFA_" unless otherwise specified with -prefix= option.
""") },

    "seqstat" : {
        "syntax1" : tabulation + 'app.Execute("seqstat sequencename output [option]")',
        "syntax2" : tabulation + 'status=app.seqstat(seqname, output, [option])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command as STAT for sequence sequencename.
The output is saved as a csv file given in second argument.
The optional parameter defines the number of statistical values computed: basic 
or main (more detailed but longer to compute).
""") },

    "seqsubsky" : {
        "syntax1" : tabulation + 'app.Execute("seqsubsky seqname degree [-prefix=xxx]")',
        "syntax2" : tabulation + 'status=app.seqsubsky(seqname, degree, [prefix=xxx])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Same command as SUBSKY but for the sequence sequencename. The output sequence 
name starts with the prefix "bkg_" unless otherwise specified with -prefix= option.
""") },

    "set16bits" : {
        "syntax1" : tabulation + 'app.Execute("set16bits")',
        "syntax2" : tabulation + 'status=app.set16bits()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Disallow images to be saved with 32 bits per channel on processing, use 16 instead.
""") },

    "set32bits" : {
        "syntax1" : tabulation + 'app.Execute("set32bits")',
        "syntax2" : tabulation + 'status=app.set32bits()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Allow images to be saved with 32 bits per channel on processing.
""") },

    "setcompress" : {
        "syntax1" : tabulation + 'app.Execute("setcompress 0/1 [-type=] [q]")',
        "syntax2" : tabulation + 'status=app.setcompress(0/1, [type=] [q])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Defines if images are compressed or not. 
0 means no compression while 1 enables compression.
If compression is enabled, the type must be explicitly written in the option 
-type= ("rice", "gzip1", "gzip2").
Associated to the compression, the quantization value must be within [0, 256] 
range. For example, "setcompress 1 -type=rice 16" sets the rice compression with 
a quantization of 16
""") },

    "setcpu" : {
        "syntax1" : tabulation + 'app.Execute("setcpu number")',
        "syntax2" : tabulation + 'status=app.setcpu(number)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Defines the number of processing threads used for calculation.

Can be as high as the number of virtual threads existing on the system, which is 
the number of CPU cores or twice this number if hyperthreading (Intel HT) is available.
""") },

    "setext" : {
        "syntax1" : tabulation + 'app.Execute("setext extension")',
        "syntax2" : tabulation + 'status=app.setext(extension)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Sets the extension used and recognized by sequences.
The argument extension can be "fit", "fts" or "fits".
""") },

    "setfindstar" : {
        "syntax1" : tabulation + 'app.Execute("setfindstar ksigma roundness")',
        "syntax2" : tabulation + 'status=app.setfindstar(ksigma, roundness)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Defines thresholds above the noise and star roundness for stars detection with 
FINDSTAR and REGISTER commands.

Sigma must be greater or equal to 0.05 and roundness between 0 and 0.9

The threshold for star detection is computed as the median of the image (which 
represents in general the background level) plus Ksigma times sigma, sigma being 
the standard deviation of the image (which is a good indication of the noise
 amplitude). If you have many stars in your images and a good signal/noise ratio, 
 it may be a good idea to increase this value to speed-up the detection and false 
 positives.

The roundness argument is the minimal ratio of the short axis on the long axis 
of the star Gaussian fit (see PSF). A value of 1 would keep only perfectly round 
stars, a value of 0.5, the default, means that stars detected twice as big on an 
axis as on the other will still be used for registration.

It is recommended to test the values used for a sequence with Siril's GUI, 
available in the dynamic PSF toolbox from the analysis menu. It may improve 
registration quality to increase the parameters, but it is also important to be 
able to detect several tens of stars in each image.
""") },

##     ''' Non scriptable 
##     "setmag" : {
##         "syntax1" : tabulation + 'app.Execute("setmag magnitude")',
##         "syntax2" : tabulation + 'status=app.setmag(magnitude)',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Calibrates the magnitude by selecting a star and giving the known apparent magnitude.
## All PSF computations will return the calibrated apparent magnitude afterwards, 
## instead of an apparent magnitude relative to ADU values.
## To reset the magnitude constant see UNSETMAG.
## """) },
##     '''
##     
##     ''' Non scriptable 
##     "setmagseq" : {
##         "syntax1" : tabulation + 'app.Execute("setmagseq magnitude")',
##         "syntax2" : tabulation + 'status=app.setmagseq(magnitude)',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Same as SETMAG command but for the loaded sequence.
## 
## This command is only valid after having run SEQPSF or its graphical counterpart 
## (select the area around a star and launch the PSF analysis for the sequence, it 
## will appear in the graphs).
## This command has the same goal as SETMAG but recomputes the reference magnitude 
## for each image of the sequence where the reference star has been found.
## When running the command, the last star that has been analysed will be considered 
## as the reference star. Displaying the magnitude plot before typing the command 
## makes it easy to understand.
## To reset the reference star and magnitude offset, see UNSETMAGSEQ.
## """) },
##     '''
    
    "setmem" : {
        "syntax1" : tabulation + 'app.Execute("setmem ratio")',
        "syntax2" : tabulation + 'status=app.setmem()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Sets a new ratio of free memory on memory used for stacking.
Ratio value should be between 0.05 and 2, depending on other activities of the 
machine. A higher ratio should allow siril to stack faster, but setting the ratio 
of memory used for stacking above 1 will require the use of on-disk memory, which 
is very slow and unrecommended.
""") },

    "setref" : {
        "syntax1" : tabulation + 'app.Execute("setref seqname image")',
        "syntax2" : tabulation + 'status=app.setref(seqname, image)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Sets the reference image of the sequence given in first argument.
""") },


    "split" : {
        "syntax1" : tabulation + 'app.Execute("split fileR fileG fileB")',
        "syntax2" : tabulation + 'status=app.split(fileR, fileG, fileB)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Splits the color image into three distinct files (one for each color) and save 
them in fileR.fit, fileG.fit and fileB.fit files")
""") },

    "split_cfa" : {
        "syntax1" : tabulation + 'app.Execute("split_cfa")',
        "syntax2" : tabulation + 'status=app.split_cfa()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Splits the CFA image into four distinct files (one for each channel) and 
save them in files.
""") },

    "stack" : {
        "syntax1" : tabulation + 'app.Execute("stack seqfilename")' + '\n' 
                  + tabulation + 'app.Execute("stack seqfilename { sum | min | max } [filtering] [-output_norm] [weighted] [-out=filename]")' + '\n' 
                  + tabulation + 'app.Execute("stack seqfilename { med | median } [-nonorm, norm=] [-filter-included] [weighted] [-out=filename]")' + '\n' 
                  + tabulation + 'app.Execute("stack seqfilename { rej | mean } [rejection type] sigma_low sigma_high [-nonorm, norm=] [filtering] [-weighted] [-out=filename]")' ,
        "syntax2" : tabulation + 'status=app.stack(seqfilename)'+ '\n' 
                  + tabulation + 'status=app.stack(seqfilename, type={sum|min|max}, [filtering] [output_norm] [weighted] [out=filename])'+ '\n'
                  + tabulation + 'status=app.stack(seqfilename, type={med|median},  [norm=no, norm=] [filter-included] [weighted] [out=filename])'+ '\n'
                  + tabulation + 'status=app.stack(seqfilename, type={rej|mean}, [rejection type], sigma_low, sigma_high, [nonorm,norm=] [filtering] [weighted] [out=filename])',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
With [filtering] being some of these in no particular order or number:
     [-filter-fwhm=value[%]] [-filter-wfwhm=value[%]] [-filter-round=value[%]]
     [-filter-quality=value[%]] [-filter-included]    

Stacks the seqfilename sequence, using options.
    o Rejection type:
        The allowed types are: "sum", "max", "min", "med" (or "median") 
        and "rej" (or "mean"). If no argument other than the sequence name is 
        provided, sum stacking is assumed.
    o Stacking with rejection:
        Types rej or mean require the use of additional arguments for rejection 
        type and values. The rejection type is one of {p[ercentile] | s[igma]
         | m[edian] | w[insorized] | l[inear] | g[eneralized] | [m]a[d]} for 
        Percentile, Sigma, Median, Winsorized, Linear-Fit, Generalized Extreme 
        Studentized Deviate Test or k-MAD clipping. If omitted, the default 
        (Winsorized) is used. The sigma low and sigma high parameters of rejection 
        are mandatory.
    o Normalization of input images:
        For med|median and rej|mean stacking types, different types of 
        normalization are allowed: -norm=add for additive, -norm=mul for 
        multiplicative. Options -norm=addscale and -norm=mulscale apply same 
        normalization but with scale operations. 
        "-nonorm" is the option to disable normalization. Otherwise addtive with 
        scale method is applied by default.

    o Other options for rejection stacking:
        "-weighted" is an option to add larger weights to frames with lower 
        background noise.
        "-output_norm" applies a normalization at the end of the stacking to 
        rescale result in the [0, 1] range.
        
    o Outputs:
        Result image name can be set with the -out= option. Otherwise, it will 
        be named as sequencename_stacked.fit

    Filtering out images:
        Images to be stacked can be selected based on some filters, like manual 
        selection or best FWHM, with some of the -filter-* options.
    See the command reference for the complete documentation on this command
""") },

    "stackall" : {
        "syntax1" : tabulation + 'app.Execute("stackall")' + '\n' 
                  + tabulation + 'app.Execute("stackall { sum | min | max } [filtering] [-output_norm] [weighted] [-out=filename]")' + '\n' 
                  + tabulation + 'app.Execute("stackall { med | median } [-nonorm, norm=] [-filter-included] [weighted] [-out=filename]")' + '\n' 
                  + tabulation + 'app.Execute("stackall { rej | mean } [rejection type] sigma_low sigma_high [-nonorm, norm=] [filtering] [weighted] [-out=filename]")' ,
        "syntax2" : tabulation + 'status=app.stackall(seqfilename)'+ '\n' 
                  + tabulation + 'status=app.stackall( type={sum|min|max}, [filtering] [output_norm] [weighted] [out=filename])'+ '\n'
                  + tabulation + 'status=app.stackall( type={med|median},  [norm=no, norm=] [filter-included] [weighted] [out=filename])'+ '\n'
                  + tabulation + 'status=app.stackall( type={rej|mean}, [rejection type], sigma_low, sigma_high, [nonorm,norm=] [filtering] [weighted] [out=filename])',
         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
With filtering being some of these in no particular order or number:
     [-filter-fwhm=value[%]] [-filter-wfwhm=value[%]] [-filter-round=value[%]]
     [-filter-quality=value[%]] [-filter-incl[uded]]

Opens all sequences in the current directory and stacks them with the optionally 
specified stacking type and filtering or with sum stacking. See STACK command for 
options description.

Stacked images for each sequence are created with the suffix "_stacked" and the 
configured FITS file extension.
""") },

    "stat" : {
        "syntax1" : tabulation + 'app.Execute("stat")',
        "syntax2" : tabulation + 'status=app.stat()',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted') + 
                    '\n' + tabulation + _("status[1] = statistics"),
        "description" : _("""\
Returns global statistic of the current image. If a selection is made, the command 
returns global statistic within the selection.
""") },

    "subsky" : {
        "syntax1" : tabulation + 'app.Execute("subsky degree")',
        "syntax2" : tabulation + 'status=app.subsky(degree)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Computes the level of the local sky background thanks to a polynomial function 
of an order degree and subtracts it from the image.
""") },

##     ''' Non scriptable 
##     "tilt" : {
##         "syntax1" : tabulation + 'app.Execute("tilt [clear]")',
##         "syntax2" : tabulation + 'status=app.tilt([clear])',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Computes the sensor tilt as the fwhm difference between the best and worst corner 
## truncated mean values. The clear option allows to clear the drawing.
## """) },
##     '''
##
##     ''' Non scriptable 
##     "unselect" : {
##         "syntax1" : tabulation + 'app.Execute("unselect from to")',
##         "syntax2" : tabulation + 'status=app.unselect( from, to)',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Allows easy mass unselection of images in the loaded sequence (from - to). 
## See SELECT.
## """) },
##     '''
##     
##     ''' Non scriptable     
##     "unsetmag" : {
##         "syntax1" : tabulation + 'app.Execute("unsetmag")',
##         "syntax2" : tabulation + 'status=app.unselect()',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Reset the magnitude calibration to 0. See SETMAG.
## """) },
##     '''
##     
##     ''' Non scriptable 
##     "unsetmagseq" : {
##         "syntax1" : tabulation + 'app.Execute("")',
##         "syntax2" : tabulation + 'status=app.unsetmagseq()',
##         "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
##         "description" : _("""\
## Resets the magnitude calibration and reference star for the sequence. See SETMAGSEQ.
## """) },
##     '''

    "unsharp" : {
        "syntax1" : tabulation + 'app.Execute("unsharp sigma multi")',
        "syntax2" : tabulation + 'status=app.unsharp(sigma, multi)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Applies to the working image an unsharp mask with sigma and coefficient multi.
""") },

    "wavelet" : {
        "syntax1" : tabulation + 'app.Execute("wavelet, plan_number, type")',
        "syntax2" : tabulation + 'status=app.wavelet(plan_number, type)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Computes the wavelet transform on "nbr_plan" plans using linear (type=1) or 
bspline (type=2) version of the 'a trous' algorithm. The result is stored in a 
file as a structure containing the planes, ready for weighted reconstruction 
with WRECONS.
""") },

    "wrecons" : {
        "syntax1" : tabulation + 'app.Execute("wrecons c1 c2 ... cn")',
        "syntax2" : tabulation + 'status=app.wrecons(list_coef)',
        "status"  : tabulation +_('status[0] = True: execution ok') + '\n' + tabulation  + _('status[0] = False: Aborted'),
        "description" : _("""\
Reconstructs to current image from the planes previously computed with wavelets 
and weighted with coefficients c1, c2, ..., cn according to the number of planes 
used for wavelet transform. 
""") },
    }

def search(name=None):
    list_cmd=helpmsg.keys()
    if name == None:
        strhelp = _("commands of pysiril/siril :\n")
        for keystr in list_cmd :
            strhelp += "     " + keystr + "\n"
        return strhelp 
    
    if name in list_cmd :
        strhelp  = _("NAME       : ") + name + "\n\n"
        strhelp += _("SYNTAXE1   : ") + "\n"   + helpmsg[name]["syntax1"] + "\n\n"
        strhelp += _("SYNTAXE2   : ") + "\n"   + helpmsg[name]["syntax2"] + "\n\n"
        strhelp += _("STATUS     : ") + "\n"   + helpmsg[name]["status"]  + "\n\n"     
        strhelp += _("DESCRIPTION: ") + "\n\n" + helpmsg[name]["description"] + "\n"      
    else:
        strhelp = _("unknown commands of pysiril/siril \n")
    return strhelp 
        