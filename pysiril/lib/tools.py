# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: pysiril ( Python SiriL )
#
# This is a library to interface python to Siril.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
import sys
import os
import re
import gettext
import locale

# ==============================================================================
def resource_path(relative_path, pathabs=None):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        #base_path = os.path.abspath(".")
        if pathabs== None :
            base_path = os.path.dirname( sys.argv[0] )
        else:
            base_path = pathabs

    return os.path.join(base_path, relative_path)

def init_language():
    try:
        if sys.platform.startswith('win'):
            if os.getenv('LANG') is None:
                lang, enc = locale.getdefaultlocale()
                os.environ['LANG'] = lang
                if len(enc) >0:
                    os.environ['LANG'] += "." + enc
        langue= os.environ['LANG'].split('.' )[0]
        if not re.search( "_" ,langue ) :
            langue = langue.lower() + "_" + langue.upper()
                
        path_abs=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        i18n_dir = resource_path( os.path.join('i18n' ,langue),pathabs=path_abs )

        if not os.path.exists(i18n_dir )  :
            gettext.install('pysiril')
        else:
            lang = gettext.translation('pysiril', os.path.dirname(i18n_dir) , languages=[langue,os.environ['LANG']] )
            lang.install()
    except Exception as e :
        print("*** error init_language():" + str(e))
        gettext.install('pysiril')

init_language()
